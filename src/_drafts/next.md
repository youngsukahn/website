---
title: Web Development - Concepts, Technologies and Libraries You Ought to Know
date: 2020-06-30
tags: ["post", "programming"]
---

There are so many concepts out there 

## JAMStack
- https://www.staticgen.com/
- GitHub/GitLab Pages

## UX/Design 

### CSS Pre-processor:
- SASS
- Less
- PostCsS
- PurgeCSS

## Development

### Programming Language
- TypeScript
- https://2019.stateofjs.com/
- https://bestofjs.org/

### UI Framework
- https://2019.stateofjs.com/
- React/Redux, Angular, Vue, Svelt

## Deployment
- Netlify
- Heroku

