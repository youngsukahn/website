---
layout: ysredsy/layout-redsy.njk
title: Young Suk Ahn Park
date: 2020-06-21
---

I am software engineer, currently working at The Walt Disney Studio as Sr. Software Architect, building digital platform that supports the studio operations.

I have a very rich background. To name a few, I have experience of consulting in South Korea, Panama and Costa Rica; starting and running my own software business; creating a non-profit organization for promoting art and culture; organizing concerts and art festivals; giving eco-tours; and researching in academic institution.

All these experiences led me to have a distinctive way of perceiving and handling challenges.

Nowadays I am in California, working for a company which I admire for its creative culture, family orientation, and inclination towards diversity and inclusion.

And when I am not at work, I enjoy spending quality time with my family, learning diverse topics and producing and sharing videos and articles of knowledge that I acquired.


## Core Values & Principles
- Respect others
- Growth
- Faith

## Experiences

## Academic Experiences
- Obtained Masters degree in Software Engineering, Carnegie Mellon University.
- Fellowship at [Institute for Software Research](https://www.isri.cmu.edu/).
- Researched at LearnLab, CMU
- Researched [Dynamic Decision Making Lab](https://www.cmu.edu/dietrich/sds/ddmlab/pastmembers.html), CMU
- Researched at Bio-informatics Lab, Seoul National University
- Obtained Bachelor degree in Computer Science and Engineering, Korea University

## Experiences Beyond Computer Science
- Founded non-profit organization, Culturaplus: Organized art exhibitions, concerts, fund-rasing for other foundations, consulted other culture-preneurs
- Taught math, programming, and Christianity to children

### Projects

- Altenia.com (sunset on 2015)
- Culturaplus (sunset on 2015)
- EmpoderemosMas (Spanish) [twitter.com](https://twitter.com/empoderemosmas), [YouTube](https://www.youtube.com/channel/UCW3h5kacfHTE74Ta2WScMMw/)


## Resources and materials about me

- [linkedin.com/in/ysahn](https://www.linkedin.com/in/ysahn/)
- [facebook.com/ysahn](https://www.facebook.com/ysahn)
- [twitter.com/seednia](https://twitter.com/seednia)
- [github.com/ysahnpark](github.com/ysahnpark/)

[More about me](./pages/more_about_me)


## Other Concepts of Interest

- Reduce, reuse, recycle: We owe to the planet and the future generation.
- Biomimicry: Billions of years of wisdom in the nature.
- Open source: Synergy of unrestricted knowledge.
- Startup & Entrepreneurship: High concentration of energy, passion and innovation
