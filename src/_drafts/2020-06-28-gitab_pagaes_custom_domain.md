---
title: GitLab Pages with Custom Domain
date: 2020-06-28
tags: ["post", "gitlab"]
---

# GitLab Pages with Custom Domain

So you have completed your new shiny static website and you are eager to have it published in the world wide web, accessible to everyone.

You have many alternatives:
- Deploying on a cloud provider: C
- Subscribing to a hosting, there are so many out there
- Obtaining a fixed IP from internet service provider (ISP) and hosting in your Raspberry PI
- Deploying to a static
-  