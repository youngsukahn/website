---
layout: redirect.njk
title: Young Suk Ahn Park
date: 2020-06-21
locale: en
---

I am an software engineer, currently working at The Walt Disney Studio as Sr. Software Architect, building a digital platform that supports the studio operations.

I have an interesting mix of experience. I have provided **software development** consultancy in South Korea, Panama and Costa Rica.  Software development is still my thing.

I had founded **my own software business**. Although it was a small consulting company, I learned a great deal of running a business, from finance to customer relationship management.  

I also created a non-profit organization for **promoting art and culture**. As an immigrant, I identified myself with two cultures, and I wanted people to respect and appreciate the different cultures.
I organized concerts and art festivals,  gave eco-tours.

Nowadays, I am dedicating my spare time to **learn** topics I wasn't too interested in during my school days but today I find them fascinating: philosophy, psychology, economics. I am making videos from my learnings in [Korean](https://www.youtube.com/channel/UCi6sTo5f7_FQDW8cvcZk1VA), [Spanish](https://www.youtube.com/channel/UCW3h5kacfHTE74Ta2WScMMw) and [English](https://www.youtube.com/channel/UCH5ychAx_0uS8zTr7_07_wQ).

All these experiences allowed me to have a distinctive way of perceiving and handling challenges.

Today I live in California, working for a company which I admire for its creative culture, family orientation, and inclination towards diversity and inclusion.

When I am not working, I enjoy spending quality time with my family, learning diverse topics
and producing and sharing videos and articles of knowledge that I have acquired.

## My Values & Principles

- Respect - Everyone is worthy of respect
- Growth - Choose uncomfortable growth over safety
- Faith - Trust God. Also, start with a premise of trust in others. 

## My current products

* [CreaSoft.dev](https://creasoft.dev/) - Developer's knowledge sharing site.
* [EmpoderemosMas.com](https://empoderemosmas.com/) - Resources on empowerment: personal growth, professional development and social betterment. (Mostly in Spanish)
* [기발가족 (YouTube)](https://www.youtube.com/channel/UCi6sTo5f7_FQDW8cvcZk1VA) - My family's Joy & Progress video blog.

## Me in Social Network

- [linkedin.com/in/ysahn](https://www.linkedin.com/in/ysahn/)
- [facebook.com/ysahn](https://www.facebook.com/ysahn)
- [twitter.com/seednia](https://twitter.com/pluspective)
- [github.com/ysahnpark](github.com/ysahnpark/)
