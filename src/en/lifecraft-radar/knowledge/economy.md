---
title: Economy

category: knowledge
date: 2020-07-25
dateRemoved:
ring: trial

description:
keywords: ["knowledge"]
homepage:
repo:

author: Young-Suk Ahn Park
---

## 2021-01-01 Trial

As per Wikipedia 'as a social domain that emphasizes the practices, discourses, and material expressions associated with the production, use, and management of resources'.

I used to say "I am an engineer, economy is not my thing." I realized that the economy is everyone's thing. At any given moment I either produce, use or manage resources.

I might as well, now the theories, implications.
