---
title: Philosophy

category: knowledge
date: 2020-07-25
dateRemoved:
ring: adopt

description:
keywords: ["Philosophy"]
homepage:
repo:

author: Young-Suk Ahn Park
---

## 2021-01-25 Trial

When I was in school, the subject of philosophy was on the bottom of my subjects-I-like list (at least it was in my list).

Now I find it fascinating.

Ethics, aesthetics, epistemology, logic, they are all relevant to daily life. 
