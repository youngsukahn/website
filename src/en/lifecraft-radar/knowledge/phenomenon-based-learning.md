---
title: Phenomenon-based Learning

category: knowledge
date: 2020-07-25
dateRemoved:
ring: trial

description:
keywords: ["awareness", "mindfulness"]
homepage:
repo:

author: Young-Suk Ahn Park
---

## 2020-07-25 Trial

Popularized by Finland education.

"In Phenomenon Based Learning (PhenoBL) and teaching, holistic real-world phenomena provide the starting point for learning. The phenomena are studied as complete entities, in their real context, and the information and skills related to them are studied by crossing the boundaries between subjects."

From [phenomenaleducation.info](http://www.phenomenaleducation.info/phenomenon-based-learning.html)