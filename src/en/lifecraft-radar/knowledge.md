---
title: Knowledge & Skills
layout: radar/radar_category.njk

this_category: knowledge

pagination:
  data: collections.radar
  size: 50
  reverse: true
  alias: radar_items
---
Topics which I'd like to acquire knowledge or gain skills.

- Topics that incites curiosities
- Skills that aligns to my values
- Acquiring knowledge
