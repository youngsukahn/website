---
title: Activities & Endeavors
layout: radar/radar_category.njk

this_category: activities

pagination:
  data: collections.radar
  size: 50
  reverse: true
  alias: radar_items
---
Where I want to focus my energy on. different from Habit, the items in  category have temporal dimension. 
It is more like a project than.

Characteristics:
- Exercising knowledge or skills
- Expanding my intellectual, spiritual, emotional territories
