---
title: First Principle

date: 2020-07-25
dateRemoved:
ring: trial

description:
keywords: ["awareness"]
homepage:
repo:

author: Young-Suk Ahn Park
---

## 2020-07-25 Trial

At TED 2013, Elon Musk was asked how he, one person, was able to achieve multiple mega projects. Elon's answer was the framework for thinking, [reasoning by first principle rather than analogy](https://www.ted.com/talks/elon_musk_the_mind_behind_tesla_spacex_solarcity/transcript?language=en#t-1099166).

> A **first principle** is a basic proposition or assumption that cannot be deduced from any other proposition or assumption. In philosophy, first principles are from First Cause. [Wikipedia](https://en.wikipedia.org/wiki/First_principle#:~:text=A%20first%20principle%20is%20a,to%20as%20postulates%20by%20Kantians.)

There are approaches similar or related to the first principle: Root Cause Analysis, the 5 Why analysis, etc.

World challenging problems cannot be solved by just analogies, comparing what others have done before.
Requires understanding the fundamental truth, the original building block, and go from there.
