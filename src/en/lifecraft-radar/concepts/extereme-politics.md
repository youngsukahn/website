---
title: Extreme Politics

date: 2020-07-25
dateRemoved:
ring: hold

description:
keywords: ["awareness"]
homepage:
repo:

author: Young-Suk Ahn Park
---

## 2020-07-25 Trial

> **Politics**: the activities of the government, members of law-making organizations, or people who try to influence the way a country is governed. [Cambridge Dictionary](https://dictionary.cambridge.org/us/dictionary/english/politics)

Politics affects all of us. People can call into heated, never-concluding discussions about politics.

If we think about it, politics is another imperfect creation of humans.

There are so many challenges in politics as a system: transparency, complex and abstruse information, low participation, polarization, conflicting incentives.

In many political debates, the arguments I heard were for argument's sake, to be in the opposing side of the opponent, rather than siding with the welfare of the people.

Rather than taking the side of parties, we should start taking the side of the people.

The word Democracy is from Greek: δημοκρατία, dēmokratiā: where dēmos means 'people' and kratos means 'rule'.
It's essence is that people have the authority to choose their government. The premise is that the government they chose has the correct quality to lead the country.

Propaganda, natural or developed bias, and superfluous information aren't helping people to make fully-rational decisions, let alone optimal decisions.

We could definitely do better with extreme politics, "politiqueria".
