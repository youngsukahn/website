---
title: Emotional Agility

date: 2020-07-25
dateRemoved:
ring: adopt

description:
keywords: ["psychology"]
homepage:
repo:

author: Young-Suk Ahn Park
---

## 2020-07-25 Adopt

"Agile" seems to be a trendy word. Industries are using the word "agile" to refer to a methodology.

Susan David, Ph.D. presented the concept of Emotional agility as a process that enables people to navigate the ups and downs of their life with acceptance and courage.

The explanation of the concept has been published in Harvard Business Review and received many praises.

I read Susan's book "EMOTIONAL AGILITY: Get Unstuck, Embrace Change, and Thrive in Work and Life".
Like many other self-help books, the book does not present a radical new solution, but proposes a simple down-to-earth four-step process.

Few concepts that did help me think differently:
1. There are no bad emotions, just uncomfortable ones. All emotions serve some purpose.
2. You can "use" the emotion for your benefit, for your growth. This requires courage and knowing your values.
3. Emotions - and feelings - will frighten you if you cannot define them.
