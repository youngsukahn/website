---
title: Circular Economy

date: 2020-07-25
dateRemoved:
ring: trial

description:
keywords: ["awareness"]
homepage: https://en.wikipedia.org/wiki/Circular_economy
repo:

author: Young-Suk Ahn Park
---

## 2020-07-25 Trial

Today's linear economy is not sustainable: a product is designed, produced and disposed of. As you can notice, the disposed products continue to accumulate. At the other end, to produce new products, new resources are needed. And we know resources on earth are not unlimited.

As [defined by wikipedia](https://en.wikipedia.org/wiki/Circular_economy),
> A circular economy (often referred to simply as "circularity") is an economic system aimed at eliminating waste and the continual use of resources.

We better start adopting this concept before it is too late. Since it is difficult to know when "too-late", until it is too-late. We should start embracing, transforming the whole economy into a circular one.

At a smaller scale, we must practice re-use, recycle, and whenever possible, reduce.
