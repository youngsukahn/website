---
title: Value Driven Approach

date: 2020-07-25
dateRemoved:
ring: adopt

description:
keywords: ["value"]
homepage:
repo:

author: Young-Suk Ahn Park
---

## 2020-07-25 Adopt

Measure your success based on your values.
