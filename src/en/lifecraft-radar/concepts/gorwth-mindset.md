---
title: Growth Mindset

category: concepts
date: 2020-07-25
dateRemoved:
ring: adopt

description:
keywords: ["awareness"]
homepage:
repo:

author: Young-Suk Ahn Park
---

## 2020-07-25 Trial

Since Carol Dweck, PhD., published her research on the growth and fixed mindset, it has influenced many fields including business management, self-help and education.

Those who believe that their intelligence or personality is something that can be developed will grow.

This reminds me of Henri Ford's quote “Whether you think you can, or you think you can't – you're right.”
