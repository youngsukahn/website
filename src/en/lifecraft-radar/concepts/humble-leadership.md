---
title: Humble Leadership

date: 2020-07-25
dateRemoved:
ring: trial

description:
keywords: ["awareness"]
homepage:
repo:

author: Young-Suk Ahn Park
---

## 2020-07-25 Trial

Is a cliche to say that the world is ever changing at a faster pace. Still, it is true.

There are many leadership styles: Democratic, Coach-style, Bureaucratic, Strategic, and many more. Now that the work are shifting towards knowledge-based, many of the leadership styles are getting outdated.

There is a style of leadership that is gaining attention: the humble (servant) leadership.

Humble leaders treat everyone with respect regardless of their position, role or title.
As explained in [[HBR 2018](https://hbr.org/2018/04/how-humble-leadership-really-works)], a servant leadership emphasizes that the responsibility of a leader is to increase the ownership, autonomy, and responsibility of followers.

Bumble leadership aligns well with psychological safety.

The humbleness must be candid, natural and consistent, not artificial or fabricated. For many, it should be challenging.

Reference:
- [How Humble Leadership Really Works](https://hbr.org/2018/04/how-humble-leadership-really-works), HBR 2018
