---
title: Habits & Behaviors
layout: radar/radar_category.njk

this_category: habits

pagination:
  data: collections.radar
  size: 50
  reverse: true
  alias: radar_items
---
Actions I would (like to) be performing at no or minimal conscious effort. 

Characteristics:
- Pattern of reactions - How I react to specific events.
- Repetitive actions (aware or unaware)
- Rituals to build habits
