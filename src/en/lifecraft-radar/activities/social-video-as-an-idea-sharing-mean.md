---
title: Social Video as a Mean for Idea-Sharing 

date: 2020-07-25
dateRemoved:
ring: trial

description:
keywords: ["social responsibility"]
homepage:
repo:

author: Young-Suk Ahn Park
---

## 2020-07-25 Trial

Everyone has ideas that are worth sharing. If you don't, I strongly suggest developing one.

In my case, there are few topics which I have been willing to share for a while: empowering people and green advocacy.

The COVID-19 "stay-at-home" policy motivated me to learn about video production and delivery through social media.

I created my channel on YouTube, and now I produce around one video per month.
At the moment the viewership is low, but still the whole process brings me the following benefits:
1. I get to know deeper on the subject as I do research.
2. I've been becoming a better communicator.
3. I keep expanding my comfort zone.
