---
title: Family Retro

date: 2020-07-25
dateRemoved:
ring: trial

description:
keywords: ["family"]
homepage:
repo:

author: Young-Suk Ahn Park
---

## 2020-07-25 Trial

The retrospectives - retro in short- were popularized in the software industry by agile practitioners. The core of the retro is continuous learning, learning from the past and improving the future.

In June 2020, I tried the first family retro with my wife and a child in late elementary. Initially my child was a little disoriented but immediately grasped the idea of the activity and started contributing with items.

All I needed was a post-its and a wall.
We did get some interesting insights on the dynamics of the family.

I think it is worth exploring a regular monthly or quarterly-based family retro.
