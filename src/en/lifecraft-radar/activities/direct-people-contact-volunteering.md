---
title: Direct People-Contact Volunteering

date: 2020-07-25
dateRemoved:
ring: adopt

description:
keywords: ["social responsibility"]
homepage:
repo:

author: Young-Suk Ahn Park
---

## 2020-07-25 Adopt

In general there are two categories of volunteering: direct and indirect.

Direct volunteering, your actions have a direct impact on the outcome. Your work directly affects people or animals. You can usually see the results immediately. An example is food delivery and distribution, tutoring, etc.

On the other hand the indirect volunteering, the effects comes later as a derivative of the work. Examples are fundraising, or administrative and clerical works.

Both volunteering works are important.

Most of my volunteering work have been initiatives at my Church, participating as a member of the committee.

Last year I started volunteering in a group that makes, delivers and distributes food for the poor. It was a vastly different experience from administrative volunteering.

The direct-contact helped me gain more empathy, and gave me more insight on the social situation that we live in.
