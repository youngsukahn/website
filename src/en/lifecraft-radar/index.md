---
title: Self Management Radar
layout: radar/radar_portal.njk

pagination:
  data: collections.radar
  size: 50
  reverse: true
  alias: radar_items
---

Inspired by [ThoughtWorks' "Technology Radar"](https://www.thoughtworks.com/radar), I created my "Life Radar."

This radar is my opinionated guide to intentionally lead myself to a more fulfilling, and satisfying life.

The blips (items) do not change as often as those in Technology Radar.

For those unfamiliar with the radar, within each category (or quadrant) there are blips placed in a specific "ring" (as in radar).

There are  four rings:
- The **Adopt** ring includes blips that I have experienced and observed positive impact. In some cases I may have not personally applied but have overwhelming evidence of the positive result.
- The **Trial** ring is for blips that I have researched about and believe that there is a compelling case for it. Testing would likely produce good results.
- The **Assess** ring consists of blips that should continue following.
- The **Hold** ring is for those things that I may want to unlearn, misconceptions that I had or things that I do in autopilot mode that I would like to stop from doing.
