---
title: Concepts & Philosophies
layout: radar/radar_category.njk

this_category: concepts

pagination:
  data: collections.radar
  size: 50
  reverse: true
  alias: radar_items
---
The foundational ideas that drive my behavior and actions.

Characteristics:
- Theories about deep subjects
- Thoughts that gets me closer to the question about significance
