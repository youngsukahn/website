---
title: Pomodoro+ Technique

date: 2020-07-25
dateRemoved:
ring: adopt

description:
keywords: ["pomodoros"]
homepage: https://deno.land
repo: https://github.com/denoland/deno

author: Young-Suk Ahn Park
---

## 2020-07-25 Adopt

Work for 20 mins, take a rest for 10 mins.
The time length can be refined.
