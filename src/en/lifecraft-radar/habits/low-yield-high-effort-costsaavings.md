---
title: Low-Yield High-Effort Cost Saving

date: 2020-07-25
dateRemoved:
ring: hold

description:
keywords: ["gratitude"]
homepage: https://deno.land
repo: https://github.com/denoland/deno

author: Young-Suk Ahn Park
---

## 2020-07-25 Hold

So many times I do something with the intent of saving money, and I end up saving a penny and wasting my precious time.
