---
title: Weekly Rretro

date: 2020-07-25
dateRemoved:
ring: adopt

description:
keywords: ["awareness", "mindfulness"]
homepage:
repo:

author: Young-Suk Ahn Park
---

## 2020-07-25 Trial

Most of the time it is hard for me to remember things I did an hour ago, not to mention the name of the person I met 30 mins ago.

My self diagnosis: Mindlessness.

There are many exercises to improve mindfulness. Googling will bring millions of results
consisting of a list of recommendations: from yoga to meditation to focusing on the senses to breathing.

The one I practiced for a few weeks is what I coined "Awareness self-talk".
This activity consists of talking to myself about major activity, even mind or emotional state if it is significant to me.
