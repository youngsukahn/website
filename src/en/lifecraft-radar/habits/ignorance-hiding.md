---
title: Ignorance-hiding

date: 2020-07-25
dateRemoved:
ring: hold

description:
keywords: ["pride"]
homepage: https://deno.land
repo: https://github.com/denoland/deno

author: Young-Suk Ahn Park
---

## 2020-07-25 Hold

During a meeting, even during friends and family gathering, I had said or stayed quiet to hide my ignorance on a topic. The reason being I did not want to look foolish or ignorant.

Better policy is to ask, show humility. It is OK to be vulnerable. You will learn and enjoy the conversation more.
