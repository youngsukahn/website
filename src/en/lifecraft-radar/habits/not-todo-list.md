---
title: Not-Todo List

date: 2020-07-25
dateRemoved:
ring: trial

description:
keywords: ["gratitude"]
homepage: https://deno.land
repo: https://github.com/denoland/deno

author: Young-Suk Ahn Park
---

## 2020-07-25 Trial

Modern days, our to-do list is full all the time. We can hardly complete the first 3, but there are 20 items in the lists.

To-do in itself is not bad, but there are things that you may want to avoid doing. Like those in the "hold" ring in this radar.

An explicit not-to-to list can help us to remember things that we do, but should save the energy for something else.

E.g. not check Facebook more than twice a day. 
