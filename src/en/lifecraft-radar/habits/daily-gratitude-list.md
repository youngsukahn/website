---
title: Daily Gratitude List

date: 2020-07-25
dateRemoved:
ring: adopt

description:
keywords: ["gratitude"]
homepage: https://deno.land
repo: https://github.com/denoland/deno

author: Young-Suk Ahn Park
---

## 2020-07-25 Adopt

At the end of the day, take 3 minutes to go over the things that I am grateful for.

It is proven that this small act increases happiness.
