---
title: Marginal Improvements

date: 2021-01-25
dateRemoved:
ring: adopt

description:
keywords: ["improvement"]
homepage:
repo:

author: Young-Suk Ahn Park
---

## 2020-07-25 Trial

Aka "Marginal gain," "the rule of 1%."

The idea is simple: start small, celebrate small wins and continue making progress.

This is the TEDx video that introduced me to the idea: [How to Achieve Your Most Ambitious Goals | Stephen Duneier | TEDxTucson](https://www.youtube.com/watch?v=TQMbvJNRpLE) 

I have the bad habit of overthinking and over-plan, which consequently overwhelms me. 

Starting small makes things more approachable, and enjoyable. Most importantly I actually get to **start doing** it.
