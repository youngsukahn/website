---
title: Thinking What To Say Next During Conversation

date: 2020-07-25
dateRemoved:
ring: hold

description:
keywords: ["gratitude"]
homepage: https://deno.land
repo: https://github.com/denoland/deno

author: Young-Suk Ahn Park
---

## 2020-07-25  Hold

Oh I do this so often. When someone is speaking, instead of listening, I start thinking how to respond or what to say next.

Whenever this happens, I am not fully grasping what the other person is saying, and in many cases, my response is out of context.
