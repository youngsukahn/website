---
title: Continuous Meta-Cognition

date: 2020-07-25
dateRemoved:
ring: adopt

description:
keywords: ["cognition", "mindfulness"]
homepage: https://deno.land
repo: https://github.com/denoland/deno

author: Young-Suk Ahn Park
---

## 2020-07-25 Adopt

Aas wikipedia puts it:
"cognition about cognition", "thinking about thinking", "knowing about knowing", becoming "aware of one's awareness" and higher-order thinking skills.

It requires effort to break away autopilot, and be aware of my actions, and avoid repeating the same inefficient cognitive process.

Akin to deliberate practice, cognitive skills such as learning, analyzing, reasoning, can be improved by being aware.

Once I make a mega-cognition an habit, the next step is to employ the strategy for the improvement, as suggested [PNAS2020].

References
[PNAS2020] Chen, P., Dweck, C., et.al. [A strategic mindset: An orientation toward strategic behavior during goal pursuit](https://www.pnas.org/content/117/25/14066), PNAS, 2020 indicates that strategic thinking can
