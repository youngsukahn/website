---
title: Second-Guessing

date: 2020-07-25
dateRemoved:
ring: hold

description:
keywords: ["gratitude"]
homepage: https://deno.land
repo: https://github.com/denoland/deno

author: Young-Suk Ahn Park
---

## 2020-07-25 Hold

Noooo! Do not second-guess!

First, it is not certain. Actually, if you are second-guessing it is because your mind is in a highly emotional state that would cloud your judgement.

Second, don't invest your energy on something that would yield no return.
