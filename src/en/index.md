---
layout: layout/page_with_posts.njk
title: Young Suk Ahn Park
date: 2021-05-04
description: Sustainability advocate. HanaLoop CTO, architect of Hana.eco platform

pagination:
  data: collections.posts_en
  size: 3
  reverse: true
  alias: posts
---

<img src="/images/20210323_la-crescenta-sky.jpg" alt="picture" >
<div class="text-gray-500 text-sm text-center align-middle">[My Neighborhood, 2021]</div>

I am an **entrepreneur, sustainability advocate, lifelong learner, educator, and software architect** with a proven track record of building multi-million dollar software systems and founding technology businesses.

Currently, I am the CTO of [HanaLoop Corp](https://www.hanaloop.com/), a climate tech company I co-founded with Hyeyeon Kim. My primary focus is on the development of an innovative sustainability platform. This endeavor involves collaborating with experts, conducting extensive research, and ensuring that the platform can create a meaningful positive impact across various sectors, contributing to a more sustainable and responsible future.

For over twenty years, I have been dedicated to building software solutions that enhance the lives of millions of people. From a system supporting the international airport in Korea to a climate change risk management and mitigation platform, I specialize in developing systems that optimize business operations and address pressing social needs.

When COVID-19 broke out in 2020, I took a hiatus from my regular job to expand my knowledge beyond technology-related topics. Due to the pandemic, numerous prestigious global conferences and symposia were held online and made accessible to the public. During this time, I observed a recurring theme that stood out: **climate change, the major upcoming disruptor**. 

Through further investigation, I discovered the concept of **"Climate Intergenerational Injustice,"** which resonated with me deeply. This revelation led me to change my priorities, dedicating myself to addressing climate change and its far-reaching impacts.

<p></p>
<hr/>

## How can I help you?

<div class="flex"><img src="/images/icon-ecological.png" style="height:30px; margin-right:.5em"> <h3>Businesses</h3></div>

- Embark on a successful **Sustainable Digital Transformation**. Keep your business competitive.
- Develop systems that are aligned with **environmental regulations**. 

<div class="flex"><img src="/images/icon-network.png" class="border-solid" style="height:30px; margin-right:.5em"> <h3>Software Development Teams</h3></div>

- Improve your **team performance** with Agile practices. Your team is your biggest asset.
- Leverage **software architecture** to tackle business problems. Architecture helps make good decisions.

<div class="flex"><img src="/images/icon-experience.png" style="height:30px; margin-right:.5em"> <h3>Individuals, Technical and Non-Technical</h3></div>

- Become a better software engineer. Expand your horizons.
- If you are a non-technical person, dabble into the world of software development. Programming will be a necessary skills for anyone.


<p></p>

<hr/>

## Related Links

- [HanaLoop.com](https://hanaloop.com/) - Green tech company that provides climate risk management platform.
- [linkedin.com/in/ysahn](https://www.linkedin.com/in/ysahn/)
- [facebook.com/ysahn](https://www.facebook.com/ysahn)
- [twitter.com/seednia](https://twitter.com/pluspective)
- [github.com/ysahnpark](github.com/ysahnpark/)

- [기발가족 (YouTube)](https://www.youtube.com/channel/UCi6sTo5f7_FQDW8cvcZk1VA) - My family's Joy & Progress video blog.
- [CreaSoft.dev](https://creasoft.dev/) - Developer's knowledge sharing site.
- [EmpoderemosMas.com](https://empoderemosmas.com/) - Resources on empowerment: personal growth, professional development and social betterment. (Mostly in Spanish)
- [pluspective.medium.com](https://pluspective.medium.com/) - My Write-ups about society, economy, technology, and other topics of relevance.

<hr/>

## Customers
<div class="flex">
  <img src="/images/cust/cust-bac-logo.png" alt="PanamaCompra" class="m-3 h-8" >
  <img src="/images/cust/cust-bbva.png" alt="PanamaCompra" class="m-3 h-8" >
  <img src="/images/cust/cust-lg_elec.png" alt="PanamaCompra" class="m-3 h-8" >
  <img src="/images/cust/cust-asep.jpg" alt="ASEP" class="m-3 h-8" >
  <img src="/images/cust/cust-panamacompra.gif" alt="PanamaCompra" class="m-3 h-8" >
</div>

<hr />
<div class="mt-4 text-gray-500 text-sm ">Icons made by <a href="https://creativemarket.com/Becris" title="Becris">Becris</a>, <a href="https://icon54.com/" title="Pixel perfect">Pixel perfect</a>, <a href="https://www.freepik.com" title="Freepik">Freepik</a>, <a href="https://www.flaticon.com/authors/iconixar" title="iconixar">iconixar</a> from <a href="https://www.flaticon.com/" title="Flaticon">www.flaticon.com</a></div>
