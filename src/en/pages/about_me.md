---
title: More About me
description: About Young Suk Ahn Park
keywords: synopsys, profile 
date: 2021-05-04
slug: /more-about-me
---

<img src="/images/20210101_family.jpg" alt="picture" class="border-solid">
<div class="text-gray-500 text-sm text-center align-middle">[Walk with my family, 2021]</div>

I am a **leader, learner, educator, and software architect**, a person blessed with a beautiful family.  I have an interesting mix of experience: developing softwares, training software engineers, running business, organizing concerts and other cultural events, selling fine arts, giving guided tours, teaching children, and of course parenting.

**Professionally**, I have been in the software building business for more than two decades. I was a consultant in distributed systems in Korea, business owner of an IT consulting company in Panama, and software architect in the United States of America. I have worked in media entertainment, education, telecommunication, finance, logistics, security, transportation, bioinformatics.  I have come to appreciate the Agile principles on valuing people, delivering frequently, understanding the customer needs and welcoming changes. 

I always keep two tenets when I work: deliver value to the customer and contribute to the growth of the organization. 

**Personally** I have always been inclined to the [Hongik Ingan](https://en.wikipedia.org/wiki/Hongik_Ingan) philosophy (홍익인간 정신). The Hongik Ingan means expanding the benefit of people, to foster the welfare of humankind. Coincidentally, [Panamanian national emblem](https://en.wikipedia.org/wiki/Coat_of_arms_of_Panama) also includes a motto "Pro Mundi Beneficio" which has the same meaning. 

As an Asian immigrant in Latin America, I experienced prejudices going from both directions, Asians towards Latin Americans and vice versa. That was the source of my first determination: to promote and facilitate the exchange of cultures, so people can appreciate different nation’s arts and customs instead of discriminating and isolating them. Today it is called diversity and inclusion. 
15 years ago, I created an organization called Culturaplus through which we organized concerts of various genres, art exhibitions in Panama and Korea, and other cultural exchange events. I was very happy when Panamanians started recognizing and appreciating the cultural differences of various countries in Asia. And I knew we made a difference when people after visiting our art exhibit booth in Korea International Art Fair 2007, sent us emails thanking us for introducing such an exquisite art. 

Nowadays, **concerned about the serious world crises** ([UN](https://www.un.org/en/global-issues/),  [WEF](https://www.weforum.org/reports/the-global-risks-report-2021), [think](https://carnegieendowment.org/topic) [tanks](http://www.millennium-project.org/projects/challenges/) and [other](https://80000hours.org/problem-profiles/) [institutions](https://borgenproject.org/top-10-current-global-issues/)), I started educating myself on topics about social divide, digital exclusion and sustainability. I am also fascinated by philosophy, psychology, and economics. Even though the topics seem unrelated, they are closely interconnected. 

As part of my learning process, I produce videos and publish them on YouTube. True to my beliefs - that knowledge should be shared for the benefit of all - all my contents and source codes are under open source license.


Today I live in California, working for a company which I admire for its creative culture, family orientation, and inclination towards diversity and inclusion.

When I am not working, I enjoy spending quality time with my family, learning diverse topics
and producing and sharing videos and articles of knowledge that I have acquired.


## Other Facts About Me 

[Here is a slide deck about me.](/files/ysahn-cs-portfolio-20210428_en.pdf)


### My Core Values

These are my core beliefs that guide the way I operate. See also: [Lifecraft Radar](/en/lifecraft-radar/).

- **Respect** - Everyone is worthy of respect.
- **Growth** - Choose uncomfortable growth over safety. Stay curious.
- **Faith** - Trust that someone infinitely wiser is supporting me in ways beyond my understanding. Also, build relationships with the premise of trust. 

### My projects

* [CreaSoft.dev](https://creasoft.dev/) - Developer's knowledge sharing site.
* [EmpoderemosMas.com](https://empoderemosmas.com/) - Resources on empowerment: personal growth, professional development and social betterment. (Mostly in Spanish)
* [기발가족 (YouTube)](https://www.youtube.com/channel/UCi6sTo5f7_FQDW8cvcZk1VA) - My family's Joy & Progress video blog.
* [Pluspective (Medium)](https://pluspective.medium.com/).

### Me in Social Network

- [linkedin.com/in/ysahn](https://www.linkedin.com/in/ysahn/)
- [facebook.com/ysahn](https://www.facebook.com/ysahn)
- [twitter.com/pluspective](https://twitter.com/pluspective)
- [github.com/ysahnpark](github.com/ysahnpark/)

### Interests

- **Sustainability, clean technology** - We cannot continue being willfully blind and ignore the negative changes happening in the environment because of human activities. 
- **Philosophy, psychology** - I was disinterested in those subjects during my school life. Now I find them fascinating and relevant in life.
- **Technologies**: Yes, I am still excited about technology, but I am now more focused on practical applications and intrinsic values.
- **Team and Organizational Performance**: Working together effectively and efficiently to achieve a common goal. When I was in college, I thought strict management and strong processes were the key for high performance, now I believe people and their interaction is critical. Here is where psychology plays an important role.
- **Learning**: Originally I used the word “education,” but rather than the systematic instruction mechanism used in schools and universities, I am more interested in creating a learning environment where children and adults maintain their motivation to learn and grow.
- **Culture and Art**: Passion for culture and art stems from my childhood experience living in Panama, from my desire for people to go beyond tolerance to the foreign and embrace and appreciate the differences. 
- **Economics and Finance**: Another subject I wasn’t too thrilled about until recently. Majority of human activities are part of the economy. To understand the world you need to understand the economy. To thrive, you need to be financially literate.


## Experiences

### Work 
- **Korea**: E&E / MethodI, Iona, Nurisol
- **Panama**: Altenia / Alianzasoft
- **United States**: Wayfair, Person Learning, The Walt Disney Studio

### Academic
- Fellowship at [Institute for Software Research](https://www.isri.cmu.edu/).
- Received [Master's degree in Software Engineering](https://mse.isri.cmu.edu/), Carnegie Mellon University.
- Researched at [LearnLab](https://learnlab.org/), CMU
- Researched [Dynamic Decision Making Lab](https://www.cmu.edu/dietrich/sds/ddmlab/pastmembers.html), CMU
- Researched at [Biointelligence Lab](https://bi.snu.ac.kr/), Seoul National University
- Received Bachelor degree in Computer Science and Engineering, Korea University

### Experiences Beyond Computer Science
- Organized art exhibitions and concerts, raised funds for other non-profit organizations, consulted artists and other culture-preneurs
- Tutored math 
- Taught programming
- Currently teaching Christianity to children
- Culturaplus (sunset on 2015)

### Personal Projects

- [CreaSoft.dev](https://creasoft.dev/), [Fundamenty](https://fundamenty.netlify.app/)
- [EmpoderemosMas](https://empoderemosmas.com/), [Twitter](https://twitter.com/empoderemosmas), [YouTube](https://www.youtube.com/channel/UCW3h5kacfHTE74Ta2WScMMw)
- Achievely.com (Coming soon)


### What Else?
I ...
- Wrote poems in the past. But nowadays, inspiration is not coming to me
- Traveled (12 different countries)
- Created artworks (paintings) 
- Traded fine arts (Raul Vasquez Saez, Doris Dalila, and other Panamanian artists)
- Gave tour guides
- Participated in food delivery to the homeless people

### I enjoy

- playing with my child (amazing things that comes out of her mind)
- watching movies
- riding bike
- traveling
- reading news & articles
- brainstorming about ideas with people

### I plan to
- visit more places around the world
- pilgrimage Way of St. James (Santiago de Compostela)
- learn new language  - programming language =)
