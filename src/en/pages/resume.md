---
title: Resume
description: Young Suk Ahn Park Resume (curriculum)
keywords: Resume, curriculum, technology leader, educator, CTO, software architect, software engineer, HanaLoop, Hana.eco, Carnegie Mellon University, Korea University 
date: 2021-05-10
slug: /resume
---


## Synopsis
I am an **entrepreneur, sustainability advocate, learner, educator, software architect**. 

Currently I am building a sustainability platform at [HanaLoop Corp](https://www.hanaloop.com/), a climate tech company I started with [my partner](https://www.linkedin.com/in/hyeyeon-kim-49534b1b/).

In the past, I have built software systems that have improved the lives of more than 200 million people[^1]. At every project, I have also drastically improved the performance of my teams, engineers and product owners. Through mentoring, coaching and establishing learning programs, I challenged them to build a better product, focusing on technical excellence, and delivering value to the customer.

My software engineering career spans more than two decades and three countries.  In Korea, as a distributed system consultant, I participated in multiple large-scale projects. In Panama, I started my own IT consulting company and led projects for the government and multinational companies. Currently, in the United States, I have been architecting and leading in-house projects and helping teams become more agile.

My other passion is contributing to society. I identify myself with the [Hongik Ingan](https://en.wikipedia.org/wiki/Hongik_Ingan), the Korean founding philosophy centered on bringing benefit to the broader humanity. Aligned to this spirit, I have created an organization for cultural promotion and exchange and taught programming and math to children. Currently, I am producing and sharing videos on YouTube about topics to help reduce the opportunity gap in society. 

From researching genetic programming for RNA structure, to leading the engineering group of multi-million budget projects with several dozen people, I have accumulated a wealth of experience as a leader and hands-on software engineer in diverse domains. This includes media entertainment, education, telecommunication, finance, logistics, security, and bioinformatics.

My vision is to bridge the gaps in society by creating products and services that empower people and contribute to thriving communities and industries.

I am people-oriented and value-focused with a track record of leading teams to deliver software products successfully resulting in great satisfaction for the stakeholders.



## Education

*   2010 ~ 2011, Master of Software Engineering (MSEng), **Carnegie Mellon University**
*   1995 ~ 1997, Bachelor in Computer Science and Engineering, **Korea University**, Korea
*   1994 ~ 1995, Computer Science, **Florida State University**, Panama Canal Branch, Panama



## Technical Skills
*  Leadership: Digital Transformation, Project Management, Learning & Development, Team Performance Improvement
*  Methodologies: Agile (Scum, XP, Kanban), Domain Driven Design, CMMi
*  Programming languages: Java, Kotlin, JavaScript/Typescript/Node.js, Python, PHP, C#, C/C++
*  Frameworks: Spring Framework, Express, Laravel, Angular, React



## Professional Experience

*   **Sr. Architect,  The Walt Disney Studios**, Burbank, USA. (10/17/2016 ~ Present).

    Devised technical strategy for the Digital Media Distribution Center of Excellence project and led the engineering team that built it.
    Lead the cross-team effort to build the studio’s software delivery platform.
    Currently I manage the studio’s engineering group’s learning and development initiative.



*   **Sr. Enterprise Architect,  Pearson**, Boston, USA. (06/26/2013 ~ 09/30/2016).

    Architected the new content management and delivery platform that consolidates multiple learning applications. Built the initial reference framework for the said platform.


*   **Software Engineer,  Wayfair LLC**, Boston, USA. (01/21/2013 ~ 06/19/2013).

    Designed and implemented a high-performing and scalable framework for the web and mobile e-commerce service.
    Partook in the design of a company-wide mentoring program.


*   **Research Engineer,  Pittsburgh Science of Learning Center**, Carnegie Mellon University, Pittsburgh, USA. (01/09/2012 ~ 01/10/2013 Part-Time).

    Extended and optimized DataShop, the world's largest repository of learning interaction data.


*   **Researcher,  Institute for Software Research (ISR)**, Carnegie Mellon University, Pittsburgh, USA. (01/09/2012 ~ 12/20/2012 Part-Time).

    Coordinated the CMU-Bosh joint research on Task-Oriented Computing for Home-Building Automation (HBA) system working together with professors and engineers from the industry. Implemented a home automation prototype that accommodates a physical space based on the context and user’s preferences.

*   **Researcher,  Dynamic Decision Making Lab**, Carnegie Mellon University, Pittsburgh, USA. (09/06/2010 ~ 12/20/2011)

    Researched Cyber Security Situational Awareness. See below for published papers.

*   **Co-founder / CTO,  Altenia Corporation**, Panama. (08/01/2005 ~ 08/30/2010)

    Ran the business. Led the development of EcoLogix™, an ERP software tailored for the Latinamerican market. Participated in projects for customers such as LG, BBVA bank, Panamanian government, and other companies.

*   **Manager,  Nuri Solutions**, Seoul, Korea. (01/02/2004 ~ 05/31/2005)

    Managed the team focusing on expanding the business to new industries. I added new sources of revenue from the telecommunication sector.

*   **Technical Account Manager / Consultant,  IONA Technologies**, Seoul, Korea. (04/01/2001 ~ 05/30/2003)

    Provided consulting of CORBA solutions and participated in various large-scale projects. Evangelized CORBA, spoke in conferences, and conducted training for engineers.

*   **Software Engineer,  METHODi**, Seoul Korea. (02/01/1999 ~ 12/23/2000) \

    Participated in projects including KT Network Enhancement and Freeway Traffic Management System.



## Selection of Past Projects


*   **Chief Architect for Digital Media Distribution Center of Excellence, The Walt Disney Studio**, USA (05/2017 ~ 12/2018) 

    The Walt Disney Studio (TWDS), which produces many blockbuster movies every year, acquired 21st Century Fox to secure the catalog of their titles in preparation for the Disney+ streaming service launch. Both studios had their own systems for license management and media distribution. With the acquisition, TWDS ended up with duplicate applications and operation groups. To revamp the outdated license management software systems and eliminate redundancies, the company initiated the Digital Media Distribution Center of Excellence (DMD CoE) project.

    The project had three specific goals:

    1. Generate more sales opportunities and increase revenue by uplifting the license management system
    2. Increase the media distribution throughput by integrating and automating the existing process
    3. Reduce operating expenses by merging redundant systems and operation groups.


    As the chief architect of the project, my responsibilities included:

    *  Identifying the gaps between current and future state and prioritizing themes within workstreams.
    *  Communicating with the stakeholders and validating the requirements.
    *  Creating and maintaining the architecture for the system’s future state. 
    *  Facilitating and guiding the interface contracts between systems.
    *  Introducing best practices to the engineering group. 
    *  Facilitating the decision-making process.
    *  Reviewing detailed designs and codes. 
    *  Driving the culture of the teams.


    There were two significant challenges in the project. First, the conflicts of interests among the stakeholders were hindering the progress. This also caused friction among engineering teams. By creating a high-level architecture and mapping them to a value stream, I aligned the stakeholders into one single vision addressing their conflicts. The second challenge was the ambiguities and inaccuracies in the license data.  The  distribution process was initiated by the license data but inaccuracies made it impossible to automate the process. I used the Domain Driven Design (DDD) to define the message structures within the boundaries to remove inaccuracies, and designed a data cleansing service to fix data integrity issues.

    My key contributions to the project were:

    1. Collaborated with the stakeholders to create the high-level architecture and technical vision
    2. Devised a technical solution for the license data restructuring, cleansing and migrating 
    3. Introduced agile principles to the team, and increased cross-team collaboration.

    Techniques and Technologies: Scrum, Domain Driven Design, Test Driven Development, OpenAPI, VueJS, Kotlin,  SpringFramework, Postgresql


*   **Architect for _Learning Content Platform_, Pearson**, Boston, USA. (05/2015 ~ 09/2016) 

    Pearson, a 170-year-old education publishing company, had acquired a variety of companies to fill the gap in online education services. The applications were not interoperable with each other, used old technology, and most importantly, lacked interactive learning features. To cope with the demands in the education industry, the company embarked on the next generation online education program.  The main objective was to provide a foundation for interactive learning content and to integrate all of its related applications.

    My role in the project was to create the core framework for interactive content and devise a plan for the integration of the applications.  Specific responsibilities included,

    1. Analyzing requirements collaborating with the content experts
    2. Creating a content specification language based on IMS/QTI standard
    3. Designing and Implementing interactive learning content platform that supports the diverse gamma of  assessment types
    4. Facilitating the adoption of the new platform


    The most difficult aspect of this project was to create a concrete system out of abstract concepts and theories of learning. This project reinforced the efficacy of well-applied agile practices in projects with a high level of ambiguity. 


    My achievements in this project includes:

    *   Produced a sound architecture for the next-gen online education system. The architecture covered the key parts of the learning content lifecycle and included a specification language, content player, and content authoring workflow.
    *  Designed and implemented the content player engine compatible with major browsers
    *  Developed a sandbox environment for content testing, which enabled rapid creation of complex contents.


    Techniques and Technologies:  Scrum, Nodejs, Hapi Framework, JavaScript, Google Closure Tool, AngularJS, Redis. 


*   **Lead Engineer, Adoption of MVC Framework, Wayfair**, USA  (02/10/2013 ~ 06/2013)

    When I joined Wayfair, it was a fast-growing e-commerce startup company. I was placed in the team that was migrating the existing ASP codes into PHP. I noticed that engineers were doing transliteration of the original code including all its spaghetti code and antipatterns. Noticing the huge technical debt, I created a Model-View-Controller (MVC) framework and reimplemented the actual feature on top of the newly created framework. I brought up the case to the leadership but received pushbacks from senior architects suggesting the performance impact and risk on the delivery schedule. I ran a performance benchmark and presented them. The data showed that my framework actually performed better thanks to the simplified page loads that resulted due to refactoring. Furthermore, the engineers agreed that the overall code was much easier to read and maintain.
    
    Based on the new insights, the leadership agreed to start a project to migrate the existing code. And all future development would use the framework from the get-go.  I lead the project transitioning initiative.


    My achievements in this project:

    1. Used evidence and data to convince the leadership about my framework.
    2. Simplified the code promoting maintainability and reducing errors in the system
    3. Improved the page load performance by at least 20% 
    4. Reduced the time required to develop new features down to 25%

    Techniques and Technologies: PHP, JMeter, MVC

*   **Team Lead / Architect, _PSP Support Tool_, Software Engineering Institute (SEI)**, Pittsburgh, USA. (09/2010 ~ 12/2011) 

    The Software Engineering Institute (SEI) needed a tool to collect metrics from engineers for the Personal Software Process<sup>TM</sup>(PSP) methodology developed at the institution.  
    As part of the master’s program practicum, a team of five, including myself, developed the PSP Support Tool. I was the team leader and came up with the architecture which all team members agreed upon.
    
    We developed a modular tool that could be extended in the future for the Team Software Process. 


    Techniques and Technologies: Java, Eclipse RCP, Hibernate, Spring Framework.

*   **Project **Lead** / Architect, _Banking Backend Integration_, BBVA Bank**, Panama. (03/2010 ~ 07/2010)

    The BBVA Bank in Panama was looking to improve the lengthy service development lifecycle. The main reason for the slow development was due to the fact that integration with core legacy banking services required tedious socket connection.

    My company was contracted to address the problem. My team and I built a service mediator that managed a pool of socket connections and exposed the SOAP / WSDL interfaces of the legacy services.  The mediator provided reliable communication between new services and the core services. The connection abstraction layer and message parsing module promoted the extensibility non-functional requirement.


    At the end of the project, the mediator provided the following benefits:

    *   Rapid application integration through  standard-based APIs
    * More reliable communication with the legacy systems
    * Service monitoring


    Techniques and Technologies: Test-Driven Development, AS400, WebSphere, IBM DB2, Java, Spring Framework, SOAP

*   **Project **Lead** / Architect, _Mobile Phone Service Quality Indicator System_, Public Service Regulation Bureau (Autoridad Nacional de Servicios Públicos)**, Panama. (01/2010 ~ 05/2010).

    Several foreign mobile carriers had entered the Panamanian market. Because the companies were busy competing for the market share, they had neglected the quality of the service. To ameliorate the situation, the government started conducting periodic  audits on the quality of their services.

    My company was selected in a government bidding for the development of an audit automation system.

    We built a web-based system that collected the quality indicators data and produced reports for the 20 different types of indicators. 

    At core, the system consisted of two components: the data schema manager and the rule engine. The data schema manager validated data structures and generated reports whereas the rule engine validated data semantics and transformed the raw input data into normalized, structured data ready for analytics and reporting.

    The personnels of the bureau were highly satisfied with the usability and productivity of the new system.


    Techniques and Technologies: .NET (C#), MVC.NET, Spring.NET, IIS, SQL Server

*   **Project Manager / Architect, _Call Center Customer Support System_, LG Electronics**, Panama. (09/2008 ~ 02/2009)

    LG Electronics Latin America was providing customer support to twelve countries in Central and South America through a call center located in Panama. The call center was using a custom built Oracle application to manage and track the service requests. The application had usability and scalability issues which hindered the performance of the agents. LG contracted my company to develop a better customer support system.

    I worked closely with the agents and managers to build a web-based application addressing their current pain points. The agents were delighted with the intuitive user interface that made it easy to navigate the graph of customer information, the products they purchased and the history of the cases. The managers appreciated the statistical information that allowed them to better allocate the resources.

    The contract was extended to add additional features to integrate the system with Computer Telephony Integration(CTI). This eliminated the need to type in the caller id into the system to retrieve their information, reducing the wait time and increasing the customer satisfaction.


    Technologies: Java, Spring Framework, MySQL, jQuery, C#, CTI

*   **Tech Lead, _Multimedia Messaging System Enhancement_, Korea Telecom**, Korea. (07/2004 ~ 10/2004)

    Korea Telecom (KT) was having reliability issues delivering multimedia messages, especially during peak times.  I joined an ongoing project of enhancing the Multimedia Messaging System (MMS) to increase the message throughput and improve the reliability of the system.

    I introduced a reliable message queue, fine-tuned the Quality of Service (QoS) and configured clustering for high availability (HA).

    After completion, the system was able to handle the highest load during the Christmas time and was considered a major milestone.


    Technologies: HP UX, C, Java, Fiorano Message Queue.

*   **Tech Lead, The _Korea International Airport’s Highway Management System_, New Airport Highway**, Korea. (11/1999 ~ 10/2000)

    The New Airport’s Highway Corporation contracted Samsung SDS to build the highway management system. Samsung SDS in turn subcontracted nine other software companies to build each of the nine subsystems.  Common Object Broker Architecture (CORBA) was selected as the communications backbone. I joined the project as a CORBA consultant and later changed my role as tech lead for the middleware subsystem.

    Within the project, I defined interfaces using Interface Definition Language (IDL) for all messages sent from subsystems to the Host. Together with the team, I implemented stubs and proxies for each subsystem. Noticing that not all engineers were familiar with the object-oriented paradigm, I conducted training sessions on the topic.

    To compensate for the lack of high availability (HA) features in the CORBA product, I implemented mechanisms to detect, route and recover from network failure. 

    I also developed an Interface Test Tool generator and distributed it to the subsystem teams to ensure high quality of the software and remove dependencies between teams.


    My contribution to the project includes:

    *  Increased the engineering group’s competency on Object-Oriented Programming skills. This improved the technical conversation across teams and increased the team's overall  productivity.
    *  Designed and implemented the high availability mechanism in the middleware making the system more reliable without additional work from each of the subsystems.


    Techniques and Technologies: Rational Unified Process, HP UX, Iona Orbix CORBA, C++, Oracle Pro*C/C++, DCOM, PowerBuilder



## Publications 

### Journals

*   Dutt, V., Ahn. Y.S., & Gonzalez, C., (2013) Cyber Situation Awareness: Modeling the Detection of Cyber Attacks with Instance-based Learning Theory. Human Factors.
*   Dutt, V., Ahn, Y.S., & Gonzalez, C., (2011). Cyber Situation Awareness: Modeling the Security Analyst in a Cyber-Attack Scenario through Instance-Based Learning. Lecture Notes in Computer Science, 6818, 280-292. doi: 10.1007/978-3-642-22348-8_24
*   Nam, J.W., Joung, J.G., Ahn, Y.S., and Zhang, B., “Two-Step Genetic Programming for Optimization of RNA Common-Structure,” EvoBio 2004, LNCS Journal. (2004.04)


### Conferences & Articles

*   Dutt, V., Ahn, Y.S., Ben-Asher, N., & Gonzalez, C. (2012). Modeling the Effects of Base-rates on Cyber Threat Detection Performance. In Paper presented at the 11th International Conference on Cognitive Modeling. Berlin, Germany.
*   Dutt, V., Ahn, Y.S., & Gonzalez, C. (2011). Cyber Situation Awareness: Modeling the Security Analyst in a cyber-attack scenario through Instance-based Learning. In Paper presented at the 25th Annual WG 11.3 Conference on Data and Applications Security and Privacy (to appear in Lecture Notes in Computer Science 6818 (LNCS 6818), Springer) . Richmond, VA, USA.
*   Dutt, V., Ahn, Y.S., & Gonzalez, C. (2011). Cyber Situation Awareness: Modeling the Security Analyst in a cyber-attack scenario through Instance-based Learning. In Poster presented at the 20th Behavior Representation in Modeling & Simulation (BRIMS) Conference. Sundance Resort, Utah, USA.
*   “EAI & Web Service Platform,” Programmer’s World Magazine, May 2002, Korea. (2002.05)



## Honors and Awards

*   12/2011 ~ 12/2012, MSE Fellowship, Carnegie Mellon University.
*   10/2010, Professional Excellence National Scholarship for Master Program, Panama Government.
*   10/2001, CEO Excellence Award, IONA Technologies.



## Certifications

*   Project Management Professional (PMI), PMP Certified (2009)



## Other Activities

*   **Empoderemos Más Media** (2020 ~ present)

    With the purpose of reducing the opportunity gaps, I started [EmpoderemosMas.com](https://empoderemosmas.com/) along with the [YouTube channel](https://www.youtube.com/channel/UCW3h5kacfHTE74Ta2WScMMw) of the same name. I publish educational and informative content and videos about current trends in digital society.  I believe everyone should be digitally literate and financially literate.

*   **Faith Formation Classes** (2013 ~ present)

    I have been teaching sunday school classes to children for over 8 years. From my experience teaching children is more difficult than training adults, since I need to be much more creative to grab their attention. One reason I teach children is because I learn and grow with them as I get to see their perspectives.

*   **Programming Classes to Children** (2014)

    I partnered with a local school to teach algorithmic thinking and programming to elementary students using Scratch, a visual programming language.

*   **Culturaplus** (2006 ~ 2010)

    I started this organization with the mission of promoting culture and facilitating cultural exchange. The organization sponsored new and emerging artists, and carried out multiple events including concerts of diverse genres, art exhibitions including in an renowned international art festival in Korea,  poem recitals and fundraising for charities.


<!-- Footnotes themselves at the bottom. -->
## Notes

[^1]:
     For example, traffic management system made driving faster and safer; multimedia messaging system allowed people to send and receive not just text but media data reliably; mobile service quality audit system improved the quality of the mobile uses in Panama; and recently the media distribution system enabled. 

