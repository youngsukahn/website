---
title: New Site - Based on Eleventy
description: Choosing Eleventy for Static Site Generation
date: 2020-06-23
tags: ["post"]
---

I finally migrated my personal website to be a static site generated with
[11ty](https://www.11ty.dev/)!

I created the original website when I was in graduate school using
[GetSimmpleCMS](http://get-simple.info/) which is based on PHP.
In the past, I used PHP a lot, mostly because it was the most ubiquitous - and cheapest -
platform to deploy a dynamic website. Now it is possible to deploy a JAM stack website
very low cost - free in most cases.

I am relieved that I moved to a static site as I no longer have to worry about:
1. My site being hacked.
2. Paying for the hosting.
3. Upgrading the underlying CMS tool.

Before deciding on 11ty, I did some research on the different Static Site Generators (SSGs)
Many different article on the topic ended pointing to the
[www.staticgen.com](https://www.staticgen.com/), which at the moment,
Nextjs, Hugo, Nuxt, Gatsby, Docusaurus, and Eleventy were in the top 6 based on the number of GitHub stars. The number of stars should not be the sole factor for choosing a tool, but
it is a good start.

From those 6 SSG tools, Gatsby was discarded because it was too heavy, Docusaurus was discarded because its focus is documentation, Nuxt was discarded because it's based on Vue (not that I dislike Vue, but I'd prefer React or otherwise framework agnostic altogether).

The competition was between Nextjs, Hugo and Eleventy.
- I read many good things about Hugo, primarily it is praised for its speed (Go-based). But ultimately the go-template was not my choice.
- Nextjs was promising, especially [since 9.3 (3/2020)](https://nextjs.org/blog/next-9-3) it added SSG support.

I ultimately chose 11ty for its simplicity and flexibility.

Just two days of using 11ty and so far I am very satisfied. It is flexible: supports multiple templates, and I can use any JS library in the npm ecosystem.



Future work:
- Add search with [Algolia](https://www.algolia.com/)
- Add comment
