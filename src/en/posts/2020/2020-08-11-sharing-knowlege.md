---
title: Sharing Knowledge
description: Reason why share my knowledge
keywords: knowledge, sharing
date: 2020-08-11
tags: ["post"]
---

I started two sites: [EmpoderemosMas.com](https://empoderemosmas.com/)  and [CreaSoft.dev](https://creasoft.dev/).

## WHY?
Everything should start with the WHY question.

### The answer is straight forward: To share knowledge

So, **why I share my knowledge?**

#### First, to learn

Not to show off that I am knowledgeable, on the contrary, because my knowledge is limited and
those things I know, the level of understanding is shallow. The process of sharing knowledge
starts with internalizing the topic so that I can articulate for the audience to understand.

As I organize my thoughts I find gaps in my knowledge, motivating me to research further. Once I
am done writing an article, I find myself with a bit more mastery in the  topic and a bit better communicator.

#### Second, to contribute

Even with my limited knowledge, I believe it is worth sharing. If it has been useful to me,
it could be useful to someone else.

#### Third, because it is fun

Learning is fun, sharing is fun. The actual mechanics writing, not so much, but that's where I am going beyond my comfort zone.
