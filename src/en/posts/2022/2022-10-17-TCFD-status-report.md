---
title: 2022 TCFD Status Report
description: Task Force on Climate-related Financial Disclosures (TCFD) recommendations
keywords: TCFD, climatedisclosure
author: YS
date: 2022-10-17
tags: ["post"]
---

For corporations, a major motivating factor for carbon reduction is cost savings. Even with rules and regulations, a for-profit company will evaluate the potential gains and losses when implementing decarbonization policies or projects. The issue is that many corporations still depend on tools that are isolated to traditional financial information, hindering effective decision-making.

In today's context, various frameworks and guidelines, such as the TCFD recommendations, can assist executives in more accurately assessing their situation by considering the climate dimension. Subsequently, they can disclose this information as a prudent business practice.

On October 13th, The Task Force on Climate-related Financial Disclosures (TCFD) released its fifth status report since the release of its final recommendations back in 2017. 
Unlike previous reports, this one includes a five-year review. What I found particularly intriguing was the section containing case studies on board oversight.

The report highlights an upward trend in the disclosure of climate-related information in financial filings. This trend is likely driven by the consistent growth in investor demand for companies to release TCFD-aligned reports.

The majority of survey respondents stated that the metrics for 'Resilience of Strategy' and 'Scope 3 GHG Emissions' were the most challenging to implement."

These changes enhance the flow and clarity of your text while maintaining its original meaning.


# Summary of Case Studies on Board Oversight

Seven case studies have been included in this year's report. Each case study comprises a description of the company's journey in adopting TCFD recommendations and a set of lessons learned.

These lessons can provide inspiration for other companies seeking to implement TCFD and enhance their preparedness for climate risks.

The most frequently repeated lesson learned is the importance of initiating governance related to climate issues at an early stage. Delaying this process could leave your company unprepared for regulatory changes. It's acceptable to start small and gradually evolve. Keep in mind that implementation and improvement might span over multiple years.

Other lessons learned and advice include the following:

- Using climate-related terminology may not effectively convey meaning to board members and executives. To capture their attention, link climate-related risks and opportunities to income and balance sheets through TCFD.
- When initiating the effort, establish a company-wide cross-functional group.
- Once the initiative is underway, ensure top-level endorsement.
- Study publicly available disclosure reports from other entities and derive insights from them.
- Participate in knowledge-sharing sessions; collective improvement is a valuable strategy.
- Implementing TCFD can serve as a foundation for adhering to other climate standards like ISSB.
- Practice transparency: reveal climate-related plans, challenges, and requirements.
- Secure engagement and support from senior-level management.
- When disclosing significant GHG emissions, include the report with climate-related commitments alongside credible and robust decarbonization strategies.
- Leverage additional tools such as CDP to translate TCFD recommendations into the most impactful disclosures

# Concluding

The statement 'Finance is a critical enabling factor for the low carbon transition, but progress on aligning financial flows with low GHG emissions pathways remains slow,' accurately summarizes the report.

Climate information disclosure entails more than just producing a report; it involves a process of collecting metrics, identifying risks, formulating strategies, and charting the path to net zero.

If your company hasn't begun disclosing climate-related information, studies indicate that initiating this process early will yield benefits. If your company is already engaged in disclosure, I recommend sharing your lessons learned and contributing to the advancement of these standards.

The original publication can be downloaded from the [TCFD site](https://www.fsb-tcfd.org/publications/).
