---
title: "COP 27 - What is it and why does it matter?"
description: COP (Conference of the Parties) is a high-profile UN climate summit held every year. The summit has been held annually since 1994 to discuss and negotiate global climate goals. The Paris Agreement in 2015 was a key milestone
keywords: COP27, climate crisis
author: YS
date: 2022-11-17
tags: ["post"]
---

Amid major geopolitical tensions, the global risk of recession, the energy crisis and record breaking climate disasters, COP27 is billed by many as a watershed moment on climate action.

|| If you think global leaders should get together periodically and work on addressing climate change, COP is precisely that venue. 

COP (Conference of the Parties) is a high-profile UN climate summit held every year. The summit has been held annually since 1994 to discuss and negotiate global climate goals. The Paris Agreement in 2015 was a key milestone.

COP27 is being held from Nov 6 through 18 in Sharm el-Sheik, Egypt. The intent of this location is to draw attention to Africa, which is one of the most vulnerable regions in the world.

Recent historic droughts and record breaking floods will hopefully serve as input for leaders to collaborate and produce concrete outcomes, and quickly move on to implementation.

COP27 started with friction while setting the agenda regarding “loss and damage.” This struggle signals a warning on how contentious conversation could be. 

> “Today a new era begins – and we begin to do things differently. Paris gave us the agreement. Katowice and Glasgow gave us the plan. Sharm el-Sheik shifts us to implementation. No one can be a mere passenger on this journey. This is the signal that times have changed,” 
> - Simon Stiell, the new Executive Secretary of the UNFCCC.



# COP26 recap
COP27 will build on the outcomes of Glasgows’ COP26. The following is a quick recap of COP26 results:
- Called on more ambitious plans for all countries
- Called upon parties to accelerate phase-down coal consumption
- Reaffirmed developed countries to fully deliver on the US$100 billion goal urgently
- Completed the Paris rulebook
- Raised the importance of “loss & damage”
In addition, there were deals and pledges on reforestation, methane reduction, and realignment of finance towards net-zero.

Topics of importance that were left unresolved include:
- Concrete details on loss and damage finance 
- Establishment of a global carbon market - Pricing the carbon in a global market
- Strong commitments to reduce coal use
 
A major disappointment was the last-minute change of the wording from "phase out" of coal-fired power, to "phase down", thus weakening the pledge.

# COP27 key areas
The crux of the summit is figuring how nations will accomplish the climate goals and materialize the financing aspects.

Three themes are expected to be prioritized.

## 1. Mitigation: How countries are reducing emissions
Aligning countries’ reduction targets to the Paris Agreement is a major goal of the summit.

Countries will present their commitment and progress, and questions such as “How ambitious are the NDC commitments?” “How transparent are the reports?” “How are countries actually reducing emissions?” will be discussed.

In the preceding COP, the Glasgow Climate Pact called for countries to strengthen their targets. The current pledges fall short on keeping the temperature below 1.5°C. The world would be likely to see a 2.5°C warming by the end of the century. As of the start of the Summit, only 29 of 193 countries followed through with their more ambitious plans.

The report from [IMF] shows a continuous upward trend in global emission after a short dip during pandemic. It is evident that the actions in prior years have not been sufficient to curb the emissions.

## 2. Adaptation: How countries will adapt

Last year, climate disasters cost the global economy an estimated $329 billion [ZURICH]. It is expected that devastating weather will become more frequent at a greater scale. Hence it has become imperative to invest in climate resiliency while we are reducing emissions.

Consider Pakistan. The country was recently impacted by a catastrophic flood with total losses estimated to be around $40 billion. Without proper infrastructure, the country will hardly break away from the climate recovery loop.

In 2009 at COP15 in Copenhagen, developed countries agreed to channel $100 billion annually by 2020 to help low income countries adapt to the climate crisis. That promise has not been met.

Although the definitions and frameworks around adaptation are far less mature than mitigation goals, further elaboration during COP27 is desired.

## 3. Financing: “Loss and damage”

Damages that cannot be avoided by mitigation or adaptation are addressed by “loss and damage” financing. This topic remained inconclusive after COP26, and by the disagreements in the agenda at the start of the summit, we can expect many obstacles in the conversation.

The rationale of “loss and damage” is that developed countries, which for that matter are also high emitting countries, should pay for the low income countries that are severely affected by climate change. Unfortunately, developing countries are the most vulnerable nations while being historically low emitters.


> “This is a fundamental question of climate justice, international solidarity and trust.”
> Antonio Guterres / United Nations Secretary-General


Tuvalu minister Simon Kofe’s speech [YouTube] at COP26 strikingly illustrated the impact of climate change on small island countries. 

Where does COP27 stand?
Scientific studies have re-asserted the severity of climate change [IPCC, WMO]. Many countries are already paying fortunes due to floods, draughts, and other devastating disasters. The stakes are high but the current settings are challenging. With the war aggravating the energy crisis,the US-China rivalry hindering global cooperation, and the escalation of tension in the Korean peninsula, the already complex problem is intensifying.

We are in a pivotal moment. If nations are unable to agree on common goals and unite the efforts, the world will reach the tipping point, causing a negative spiraling effect to unfold.

COP27 will surely result in global agreements and deals that will be translated into normatives. Meanwhile, organizations should not wait until the regulations are finalized. Given the urgency, corporates should proactively take climate initiatives, make plans and put them into action.

At the end of the day, success of COP will be measured by the resulting actions and deliveries.
