---
title:  Korea Children's Day 2023
description: Letter to my daughter
keywords: children, 
author: YS
date: 2023-05-05
tags: ["post"]
---

To SJ, 

It’s Children's day in Korea! 
You are 14 now, so technically you passed the range. Nonetheless, for us, you will be our beloved child forever..

<div class="p-2 border border-gray-500 bg-gray-200">
<h3>Story: The eagle and its lost identity.</h3>

Near the end of winter, the chicken farmer went to the woods to gather logs. As he was passing the tallest tree in the area he noticed something wriggling at the bottom of the tree. He found out it was an eagling.
He felt sorry for the little bird and brought it back to the farm and put it with the other birds in the farm: the chickens.

The eagling grew among the chickens, ate with them, played with them, and slept with them.
One sunny day the eagle noticed a shadow while it was pecking earthworms and grains off the floor. It looked up and noticed an eagle, flying in the high sky. Its iris grew and sparked for a moment, then it looked at its peers and went back to busily pecking the ground.
</div>

That’s a story my high school teacher told us and encouraged us to realize that we are all eagles.

Society has conditioned us with a certain definition of success, one that equates greatness with accumulating wealth and power, exalting one's name. We believe that those are the ones worth emulating as role models.

My definition of success is **living a life that is in harmony with our personal values and goals**. True greatness can be found in those who are able to fulfill their own unique roles. Paulo Coelho's 'Personal Legend', as presented in The Alchemist, bears resemblance.

I believe chickens that fulfill their role achieve greatness. Likewise earthworms that fulfill their role achieve the same greatness as eagles that complete their respective roles.

That said, I also believe that there are eagles amongst chickens, unbeknownst of their own identity. 
I firmly believe that you are an eagle, not in terms of its majestic appearance, but in terms of  your exceptional resilience and ability to endure and overcome obstacles, and your destiny to rise high, beyond conventions.

You will experience a **great sense of fulfillment and joy when you face and overcome challenges**. Go and seek those challenges. Circumstances will come where you will push yourself to the limit and **you will find that the limit was a mere illusion**. 

Your biggest fear, your biggest barrier is yourself. Overcome those, sooner the better.
Keep pushing yourself to the limits. One day you will face your true limit. That limit will not only be yours to solve but humanity’s.
And remember, great challenges are rarely overcome by individuals alone, but rather through collaboration and teamwork.

<div class="p-2 border border-gray-500 bg-gray-200">
May you continue to approach life with the same curious, fearless and adventurous spirit you had as a child.
</div>


Your dad, too, is working with a bold and adventurous spirit for a sustainable planet to pass on to you and your generation.



<div class="mt-5 space-y-2">
<img src="/images/posts/sj-20221230_121934.jpeg" class="border-8 border-gray-200">
<img src="/images/posts/sj-20210911_175148.jpeg" class="border-8 border-gray-200">
<img src="/images/posts/sj-20210704_111625.jpeg" class="border-8 border-gray-200">
<img src="/images/posts/sj-20210321_173508.jpeg" class="border-8 border-gray-200">
</div>
