---
title: IFRS Sustainability Disclosure Standards

description: "The IFRS Sustainability Disclosure Standards are important because they consolidate multiple other disclosure standards and are actively working with others to maintain interoperability." 
keywords: IFRS, ISSB, CSDB
author: YS
date: 2023-07-03
tags: ["post"]
---

## Key Points

- The International Sustainability Standards Board (ISSB) was created in response to the global demand for consistent, comparable, and verifiable sustainability reports useful for investors and stakeholders.
- The IFRS Sustainability Disclosure Standards are important because they consolidate multiple other disclosure standards and are actively working with others to maintain interoperability.
- ISSB released two standards in June 2023: S1 - General Requirements for Disclosure of Sustainability-related Financial Information, and S2 - Climate-related Disclosures.
- For organizations, complying with ISSB standards implies putting in place teams, processes, policies, and technologies to support governance, strategy, risk management, and metrics and targets related to sustainability and climate. 
- Adopting ISSB can bring about transparency and accountability, attracting investors, talents and new customers. 



A comprehensive consultation with diverse stakeholders representing various geographical regions, highlighted the pressing global need for more consistent, comparable and verifiable information about companies’ sustainability‐related risks and opportunities.

In 2021, the Trustees of the IFRS Foundation announced the creation of the International Sustainability Standards Board (ISSB) with the objectives to develop standards for a global baseline of sustainability disclosures that meet investors' needs. It also aims to assist companies in providing such disclosures and facilitate interoperability and adoption of the standards.

ISSB S1 and S2 are the first two disclosure standards released in June this year.

# Why is IFRS SDS Significant?

In simple terms, the ISSB builds on the Task Force on Climate-related Financial Disclosures (TCFD) and brings together various global sustainability disclosure standards: SASB, Integrated Reporting (IR), and CDSB. Additionally, it collaborates with the Global Reporting Initiative (GRI) to ensure interoperability. 

In short, the resulting unified framework will facilitate comprehensive and consistent sustainability reporting, eliminating duplicative reporting, and help companies communicate worldwide cost-effectively. Single standard for companies to disclose, easier for investors to understand and compare.

Given that IFRS financial reporting is used by over 140 jurisdictions, the ISSB SDS will catalyze a global shift towards more standardized and transparent sustainability reporting practices across industries and regions.

# What does IFRS SDS consist of?

On June 26 2023, ISSB released two Sustainability Disclosure Standards (ISSB Standards). The IFRS S1 is the General Requirements for Disclosure of Sustainability-related Financial Information. The IFRS S2 is the Climate-related Disclosures and it integrates the recommendations of the Task Force on Climate‐related Financial Disclosures (TCFD) and requires the disclosure of information about both **cross‐industry and industry‐specific climate‐related risks and opportunities**.

IFRS S1 and IFRS S2 require organizations to disclose all decision-useful, material information that is sustainability-related and climate-related. This includes risks and opportunities that could reasonably be expected to affect its ‘prospects’—its cash flows, access to finance or cost of capital over the short, medium or long term.

Both standards use the same four areas from TCFD: 
1. Governance - Information aiding investor comprehension of a company's governance processes, controls, and risk management.
2. Strategy - Information for investor insight into a company's risk and opportunity management strategy.
3. Risk Management - Information for investor understanding of a company's risk and opportunity processes.
4. Metrics and targets - Information for investor insight into a company's performance relative to risks, opportunities, and target progress.

IFRS S1 uses SASB as the starting point for the identification of material risks and opportunities. The standards also consider the CDSB framework and other risks identified by the industry.

IFRS S2 is concerned with two types of climate-related risks: physical risk - those resulting from increased severity of extreme weather; and transition risks - those associated with policy action and changes in technology.

IFRS S2 is also consistent with the four areas defined in TCFD: governance, strategy, risk management, and metrics and targets. However, unlike S1, it is climate-specific. For example, the disclosure should include information such as changes to its business model due to climate risks, adaptation or mitigation efforts, plans for transitioning to a lower‐carbon economy, and measures for meeting climate-related targets.

It is noteworthy that ISSB stressed out the importance of scope 3 and disclosing the emission related to companies value chain.  Companies with activities in asset management, commercial banking or insurance are required to disclose their ‘financed emissions’.
The ISSB developed a Scope 3 measurement framework to provide additional guidance about the relevant information to use in measuring Scope 3 GHG emissions.

# What does IFRS SDS mean to organizations?

The ISSB is effective for annual reporting periods beginning on or after January 1st, 2024, with the first-year transitional relief for scope 3 reporting. Currently, the ISSB is cooperating with different jurisdictions for the adoption of S1 and S2.

The main implication of ISSB for organization is that they will need to implement the four areas of TCFD namely governance, strategy, risk management, and metrics and targets.

First, they will need to start collecting information about their sustainability-related risks and opportunities. CDP is already aligned with TCFD, and GRI and ISSB have committed to work on the interoperability of the standards. Entities already disclosing GRI and/or CDP reports may require small adjustments to their reporting. Otherwise organizations will need to make changes to their internal reporting processes to incorporate sustainability information gathering and verification.

An important consideration is the disclosure of emissions in the value chain, known as scope 3. Even with a year for transitional relief, the measurement of scope 3 emissions, due to its vast amount of data and the need for partner collaboration, requires significant time for setting the process and data infrastructure.

Second, organizations will need to start managing their sustainability-related risks and opportunities, formulating strategies, and tracking execution leading to the reduction target. This means that organizations will need to establish climate governance, develop new policies and procedures, and invest in skills and new technologies.

Third, organizations will need to start disclosing this information to their investors and other stakeholders. This means that organizations will need to make changes to their annual reports and other public disclosures and continuously address feedback on the disclosure.

Once the organization establishes processes in line with ISSB standards, it significantly enhances its transparency and accountability. This, in turn, can attract new investors and top talent while also appealing to customers.

# Conclusion

Nowadays, sustainability-related risks are financial risks. They can directly impact the cash flow and the overall competitiveness of the organization. Ensuring robust risk management strategies and sustainable practices is crucial for safeguarding financial stability and long-term success in today's business environment.

ISSB standards can benefit investors, stakeholders, and companies in a number of ways.
For investors the ISSB standards can provide more information about risks and opportunities. This information can help investors to make decisions about where to invest their money.
For stakeholders, the ISSB standards can help them to hold companies accountable for their sustainability performance
For companies, the ISSB standards can help them to improve their sustainability performance ensuring better climate mitigation and adaptation.

Adopting ISSB standards early on can alleviate the company's sustainability reporting burden, providing a buffer for trial and error as they adjust to the new requirements.
