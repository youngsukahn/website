---
title:  Abordando el cambio climático - Preparándonos para el futuro donde el clima es un factor clave
description: El efecto del cambio climático que nos impacta hoy y mañana. Instrumentos de iniciativas y regulaciones en actualidad. Contribuyendo a la acción climática
keywords: cambio climatico, 
author: YS
date: 2023-11-27
tags: ["post"]
---

<img src="/images/posts/2023/20231206-Panama-FMYE-cambio_climatico-banner.jpeg" class="border-gray-200">

El 6 y 7 de diciembre, presentaré en el taller el tema que me cautiva profundamente: **el cambio climático**.

Como co-fundador de la empresa coreana especializada en el desarrollo de sistemas de gestión de gases de efecto invernadero, he tenido la valiosa oportunidad de interactuar con profesionales del sector y acceder a información que ha transformado mi enfoque hacia la problemática.

El **primer paso hacia la acción climática implica adquirir y difundir conocimientos** de manera efectiva. 

Así pues le invito que participe en el taller, donde la donacion ira para la entrega de regalos a niños en esta navidad. 

El evento es organizado por la Fundación Mujer y Energía.

Registro: [https://forms.gle/2WpiZY1MrFJxqjJe6](https://forms.gle/2WpiZY1MrFJxqjJe6)

Para mayor información: fundacionmujeryenergia@gmail.com 

– Abajo la información del taller – 

¡Te invitamos a participar de nuestro taller con propósito!

"Abordando el cambio climático:
Preparándonos para el futuro donde el clima es un factor clave"

- Día: 6 y 7 de diciembre 2023
- Hora: 5:00pm a 6:30pm de Panamá
- Dirigido: Todo público de Latinoamérica
- Plataforma: Zoom
- Expositor: Young-Suk Ahn Park, Co-fundador de HanaLoop
- Costo: $20.00
*Con derecho a certificado digital.

**Con la compra de tu boleto, regalaras un juguete en reyes magos a:

- Campaña Juguete Pendiente de @4evercaringfoundation
- Parroquia Sagrado Corazón de Jesús
- Comunidad de Gosguna en Veracruz
- Hospital del Niño

¡Ayúdanos a llegar a la meta de 200 juguetes!

Para recibir información regístrate aquí: [https://forms.gle/2WpiZY1MrFJxqjJe6](https://forms.gle/2WpiZY1MrFJxqjJe6)

Cierre de inscripciones hasta el 4 de diciembre de 2023.

#latam #navidad #oportunidad

<img src="/images/posts/2023/20231206-Panama-FMYE-cambio_climatico-1.jpeg" class="">
<img src="/images/posts/2023/20231206-Panama-FMYE-cambio_climatico-2.jpeg" class="">
<img src="/images/posts/2023/20231206-Panama-FMYE-cambio_climatico-3.jpeg" class="">
<img src="/images/posts/2023/20231206-Panama-FMYE-cambio_climatico-4.jpeg" class="">
<img src="/images/posts/2023/20231206-Panama-FMYE-cambio_climatico-5.jpeg" class="">
