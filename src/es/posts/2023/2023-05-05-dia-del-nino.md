---
title:  Dia del niño en Corea 2023
description: Letter to my daughter
keywords: children, 
author: YS
date: 2023-05-05
tags: ["post"]
---

Para mi hija, 

¡Es el día del niño en Corea!
Ahora tienes 14 años, así que técnicamente pasaste el rango. 
Sin embargo, para nosotros, serás nuestra niña amada para siempre.

Comenzaré la carta con un cuento que escuché cuando tenía tu edad.

<div class="p-2 border border-gray-500 bg-gray-200">
<h3>Cuento: El águila y su identidad perdida.</h3>

Cerca del final del invierno, el granjero fue al bosque a recoger troncos secos. Mientras pasaba junto al árbol más alto de la zona, notó que algo se retorcía en la parte inferior del árbol. Descubrió que era un águila bebé.
Sintió pena por el ave y lo trajo a la granja y lo puso con las gallinas.

El águila creció entre las gallinas, comió con ellas, jugó con ellas y dormió con ellas.

Un día soleado, el águila notó una sombra pasar rapidamente mientras picoteaba granos y lombrices del suelo. Miró hacia arriba y notó un águila, volando en el alto cielo azul. Su iris creció y brilló por un momento, luego miró a sus compañeros y volvió a picotear afanosamente el suelo.
</div>

Esa fue una historia que nos contó mi maestra de secundaria animandonos a darnos cuenta de que somos águilas.

La sociedad nos ha condicionado con una definición particular de éxito: la de lograr fama acumulando riqueza y poder. Creemos que aquellos con dinero y poder son los que vale la pena tomar como modelos a seguir.  Yo había compartido las mismas normas, pero mis creencias han cambiado.

Ahora mi definición de éxito es **vivir una vida que esté en armonía con nuestros valores y metas personales**. La verdadera grandeza se puede encontrar en aquellos que son capaces de cumplir con sus propios roles únicos. La frase 'Leyenda personal' de Paulo Coelho, me parece una buena descripción.

Yo creo que las gallinas que cumplen su rol alcanzan la grandeza. Así mismo las lombrices que cumplen su función alcanzan la misma grandeza que las águilas que cumplen sus respectivas funciones.

Dicho esto, también creo que hay águilas entre los gallinas, sin estar anuentes de su identidad.

Yo creo firmemente que eres un águila, no por la apariencia majestuosa, sino por tu excepcional resiliencia y habilidad para soportar y superar obstáculos, y tu destino a elevarte más allá de las convenciones.

Experimentarás una **gran sensación de realización y satisfaccion cuando enfrentes y superes desafíos**. Ve y busca esos desafíos. Llegarán circunstancias en las que te esforzarás hasta el límite y **duscubriras que el límite era una mera ilusión**.

Tu mayor miedo, tu mayor barrera eres tú mismo. Superarlos, cuanto antes mejor.

Sigue empujándote hasta los límites. Un día te enfrentarás a tu verdadero límite. Ese **límite no solo será tuyo para resolver, sino de la humanidad**.
Y recuerda, los **grandes desafíos rara vez los superan los individuos solos**, sino a través de la colaboración y el trabajo en equipo.

<div class="p-2 border border-gray-500 bg-gray-200">
Vive la vida con el mismo espíritu curioso, intrépido y aventurero que tenías cuando eras una niña.
</div>

Yo tambien estare trabajando arduamente, con curiosidad y espíritu aventurero, para ayudar a crear un planeta sostenible que pueda ser heredado a ti y las generaciones futuras.




<div class="mt-5 space-y-2">
<img src="/images/posts/sj-20221230_121934.jpeg" class="border-8 border-gray-200">
<img src="/images/posts/sj-20210911_175148.jpeg" class="border-8 border-gray-200">
<img src="/images/posts/sj-20210704_111625.jpeg" class="border-8 border-gray-200">
<img src="/images/posts/sj-20210321_173508.jpeg" class="border-8 border-gray-200">
</div>
