---
title: More About me
date: 2020-06-21
slug: /more-about-me
---

Soy afortunado de tener una familia que me me da fuerza espiritual y emocional.

He vivido en Corea, Panamá y Estados Unidos. Actualmente resido en Corea del Sur.

## Mis principales valores

- Respeto
- Crecimiento
- Fe

## Yo en

- [linkedin.com/in/ysahn](https://www.linkedin.com/in/ysahn/)
- [facebook.com/ysahn](https://www.facebook.com/ysahn)
- [twitter.com/seednia](https://twitter.com/seednia)
- [github.com/ysahnpark](github.com/ysahnpark/)

## Experiencias

### Trabajos anteriors
- Nuevamente en Corea (2021~): [HanaLoop Corp](https://hanaloop.com/)
- Estados Unidos (2010~2021): Wayfair, Person Learning, The Walt Disney Studio
- Panamá (2005~2010): Altenia / Alianzasoft, Culturaplus-Arteplus
- Corea (2003~2005): E&E, Iona, Nurisol

### Experiencas Académicas

- Magister en Ingeniería de Softwware, Carnegie Mellon University.
- Fellowship en [Institute for Software Research](https://www.isri.cmu.edu/).
- Investigador en LearnLab, CMU
- Investigador [Dynamic Decision Making Lab](https://www.cmu.edu/dietrich/sds/ddmlab/pastmembers.html), CMU
- Investigador en Bio-informatics Lab, Universidad Nacional de Seúl
- Bachiller en ciencias y ingeniría computacional, Universidad de Corea

### Experiences fuera del ambito de informatica
- Fundador de organización sin fins de lucro: Culturaplus. Promovimos el arte y la cultura organizando exposiciones de arte, conciertos y otras actividades culturales.
- Tutor de matemáticas, programación y religión.

### Proyectos

- Altenia.com (sunset on 2015)
- Culturaplus.net (sunset on 2015)
- [EmpoderemosMas](https://empoderemosmas.com/), [Twitter](https://twitter.com/empoderemosmas), [YouTube](https://www.youtube.com/channel/UCW3h5kacfHTE74Ta2WScMMw/)
- [CreaSoft.dev](https://creasoft.dev/), [Fundamenty](https://fundamenty.netlify.app/)


## Que más?
Yo ...
- Ecribi poesias
- viaje a 14 different paises
- pinte
- Vendi obras de artes de artistas panamenos incluyendo Raul Vasquez Saez, Doris Dalila, Osvaldo Herrera Graham y otros artisas panameños
- organicé conciertos de jazz y opera
- fui guia turistica
- produje videos

### Disfruto

- conversar con gentes de nuevas ideas
- jugar on mi hija
- ver peliculas
- andar por bicicletas
- viajar
- leer libros, articulos

### Planeo

- visitar mas paises
- camino de Santiago de Compostela
- continuar aprendiendo
