---
title: Resume
date: 2020-06-21
slug: /resume
---

**Young Suk Ahn Park** | Co-fundador / CTO [HanaLoop Corp](https://hanaloop.com/)

## Synopsis

Lidero el desarrollo de soluciones tecnológicas innovadoras que impulsan la transición hacia una economía baja en carbono. Con más de dos décadas de experiencia en ingeniería de software, he liderado equipos multidisciplinarios en la creación de plataformas digitales escalables y de alto impacto.

En HanaLoop, he desarrollado la plataforma de sostenibilidad [hana.eco](https://www.hana.eco) que ha permitido a nuestros clientes reducir sus emisiones de carbono y cumplir con las regulaciones. Mi experiencia en diversas industrias, desde la logística hasta la bioinformática, me permite abordar los desafíos de sostenibilidad desde una perspectiva holística.

Soy un apasionado defensor de la filosofía "Hongik Ingan" y estoy comprometido a utilizar mi conocimiento y habilidades para crear un futuro más sostenible para todos. Mi visión es empoderar a las personas y organizaciones a través de la tecnología, construyendo un mundo donde la prosperidad económica vaya de la mano con la protección del medio ambiente.



## Educación

- 2012 ~ 2013, Investigador (Fellowship), Instituto de Investigación de Software, Universidad Carnegie Mellon.
- 2010 ~ diciembre de 2011, Máster en Ingeniería de Software, Universidad Carnegie Mellon (QPA: 3.8).
- 1995 ~ 1997, Licenciatura en Ciencias de la Computación e Ingeniería, Universidad de Corea, Corea.
- 1993 ~ 1995, Ciencias de la Computación, Universidad Estatal de Florida, Sede del Canal de Panamá, Panamá.


## Idiomas

Fluido en español, coreano e inglés.


## Experiencia Laboral
- HanaLoop, Seúl, Oficial de technologia. (09/2021 ~ presente)

  Lidero el desarrollo de la plataforma de sostenibilidad.

- Disney Studio, Glendale, Arquitecto Jefe de Software. (10/2016 ~ 12/2021)

  Desarrollé la arquitectura del Media Distribution Platform y lidere la coordinacion del proyecto. 
  
- Pearson, Boston, EE. UU. Arquitecto de Software Senior. (06/2013 ~ 10/2016)
 
  Desarrollé el framework de interfaz de usuario interactiva para plataforma de education en linea.

- Wayfair, Boston, EE. UU. Ingeniero de Software. (02/2013 ~ 06/2013)

  Diseñé e implementé de el framework de servicio web/móvil de alto rendimiento.

- Human Computer Interaction Institute, Universidad Carnegie Mellon, Pittsburgh, EE. UU. Investigador Asociado/Ingeniero de Software. (01/2012 ~ 01/2013)

  Desarrollé plataforma DataShop, sistema de minería de datos para análisis de aprendizaje.

- Dynamic Decision Making Lab, Universidad Carnegie Mellon, Pittsburgh, EE. UU. Asistente de Investigación. (01/2011 ~ presente).
  
  Contribuí en la investigación sobre Conciencia Situacional en Ciberseguridad. Publiqué dos artículos (ver más abajo).

- Altenia Corporation, Panamá. Fundador y Consultor Principal. (08/2005 ~ 2015)
  
  Serví como consultor de informática. Clientes: gobierno panameño, LG, varias otras PYMEs.

- Nuri Solutions, Seúl, Corea. Gerente de la División de Desarrollo de Nuevos Negocios. (01/2004 ~ 05/2005)

  Encargado del desarrollo de nuevos negocios utilizando productos Analysis de textos y ESB.

- Bioinformatics Lab, Universidad Nacional de Seúl, Corea. Asistente de Investigación. (06/2003 ~ 12/2003).
  
  Asistencia en la investigación sobre evolución del ARN con programación genética. Publicado un artículo (ver más abajo).

- IONA Technologies, Seúl, Corea. Gerente de Cuenta Técnica/Consultor. (04/2001 ~ 05/2003).
  
  Provisión de servicios de consultoría de productos CORBA para clientes como Samsung, Korea Telecom y SK Telecom. También realizó capacitación para socios y clientes.

- METHODi, Seúl, Corea. Ingeniero de Software. (02/1999 ~ 12/2000).
  
  Desarrollo de proyectos incluyendo Aeropuerto Internacional y SK Telecom.


## Experiencia en Proyectos
- Investigador en Informática Orientada a Tareas en el Proyecto de Automatización de Edificios del Hogar, ISR/CMU, Pittsburgh, EE. UU. (01/2012~12/2012).

  Investigactión de automatización y optimización de tareas en un entorno de hogar y edificio.

- Gerente de Proyecto/Arquitecto, Proyecto: Mediador de Servicios para Banca en Línea, Banco BBVA, Panamá. (03/2010~07/2010)

  Desarrollo de un sistema de middleware basado en Arquitectura Orientada a Servicios (SOA) para integrar aplicaciones de back-end con aplicaciones de canal. El sistema maneja un promedio de 400 transacciones por minuto. Tecnologías: AS400, Web Sphere, IBM DB2, Java, Servicios Web, Spring Framework, XML.

- Gerente de Proyecto/Arquitecto, Proyecto: Sistema de Medición de Indicador de Calidad de Servicio de Telefonía Móvil, Autoridad Nacional de Servicios Públicos (ASEP), Panamá. (01/2010~05/2010)

  Desarrollo de una aplicación web que recopila datos de métricas de calidad de servicio telefónico y proporciona informes para auditoría. 

- Gerente de Proyecto/Arquitecto, Proyecto: Sistema de Gestión de Soporte al Cliente para Call-center, LG Electronics, Panamá. (09/2008~02/2009)

  Desarrollo de un sistema de gestión de tickets que reemplazó el sistema basado en cliente-servidor existente. El nuevo sistema basado en web se integró sin problemas con el sistema CTI (Telefonía). Manejó 150 usuarios concurrentes (agentes), incluyendo gerentes regionales de 12 países diferentes en América Latina. El nuevo sistema funcionó 8 veces más rápido que el anterior. Tecnologías: Java, Spring Framework, MySQL, jQuery, Hessian, C# (para aplicación CTI).

- Gerente de Proyecto, Proyect: Sistema de Gestión de Expo y Recinto, CAPAC, Panamá. (03/2006~11/2006)

  Desarrollo de un sistema que gestionó el sitio web para la feria anual de bienes raíces y construcción organizada por la organización.

- Desarrollador, Proyecto: Sistema de Interfaz de Información Crediticia (CIIS), HSBC Corea, Corea. (10/2004~02/2005)

  Desarrollo de un módulo en CIIS que proporcionó una interfaz de comunicación segura entre las sucursales de Corea y Hong Kong. 

- Programador Principal, Proyecto: Mejora del Sistema de Mensajería Multimedia (MMS), Korea Telecom, Corea. (07/2004~10/2004)

  Desarrollo de un sistema de integración de alto rendimiento que proporcionó comunicación confiable entre aplicaciones que manejaban mensajes multimedia móviles. 

- Arquitecto, Proyecto: Integración en el Sistema de Gestión de Autopistas del Aeropuerto Internacional de Incheon, Corea. (11/1999~10/2000)

  Desarrollo de un bus de servicio basado en CORBA que integró nueve sistemas que gestionaban la autopista hacia el nuevo Aeropuerto Internacional. Varios mecanismos de tolerancia a fallos que desarrollé fueron fundamentales para el éxito del proyecto en general.

- Líder de Proyecto, Proyecto: Sistema de Comunidad online, Ipopcorn, Seúl, Corea. (08/1999~11/1999)
  
  Dirigí un equipo que desarrolló un portal de comunidad. El portal incluyó gestión de usuarios, foro, chat e integración con la plataforma de comunidad de realidad virtual de Blaxxun. Tecnologías: ASP, SQL Server, VRML.


## Publicaciones
- Dutt, V., Ahn, Y.S., & Gonzalez, C. (2011). Cyber Situation Awareness: Modeling the Security Analyst in a Cyber-Attack Scenario through Instance-Based Learning. Lecture Notes in Computer Science, 6818, 280-292. doi: 10.1007/978-3-642-22348-8_24
- Dutt, V., Ahn, Y.S.., & Gonzalez, C. (in preparation). Cyber Situation Awareness: Modeling the Detection of Cyber Attacks with Instance-based Learning Theory.
- Dutt, V., Ahn, Y.S.., Ben-Asher, N., & Gonzalez, C. (2012). Modeling the Effects of Base-rates on Cyber Threat Detection Performance. In Paper presented at the 11th International Conference on Cognitive Modeling. Berlin, Germany.
- Dutt, V., Ahn, Y.S.., & Gonzalez, C. (2011). Cyber Situation Awareness: Modeling the Security Analyst in a cyber-attack scenario through Instance-based Learning. In Paper presented at the 25th Annual WG 11.3 Conference on Data and Applications Security and Privacy (to appear in Lecture Notes in Computer Science 6818 (LNCS 6818), Springer) . Richmond, VA, USA.
- Dutt, V., Ahn, Y.S., & Gonzalez, C. (2011). Cyber Situation Awareness: Modeling the Security Analyst in a cyber-attack scenario through Instance-based Learning. In Poster presented at the 20th Behavior Representation in Modeling & Simulation (BRIMS) Conference. Sundance Resort, Utah, USA.
- Nam, J.W., Joung, J.G., Ahn,Y.S., and Zhang, B., Two-Step Genetic Programming for Optimization of RNA Common-Structure, EvoBio 2004, LNCS Journal. (2004.04)
“EAI & Web Service Platform,” Programmer’s World Magazine, May 2002, Korea. (2002.05)

## Honores y Premios

- 12/2011 ~ 12/2012, MSE Fellowship, Universidad Carnegie Mellon.
- 2010, Beca Nacional de Excelencia Profesional para Programa de Maestría, Panamá.
- 10/2001, Premio al CEO, IONA Technologies.


## Presentaciones

- "Impacto del Cambio Climático en Industria Cosmetica", Corea, Junio 2024
- "Regulaciones, Iniciativas Climáticas y el Mercado de Carbono", Manaus, Junio 2024
- "Navigating Sustainability Reporting Standards and Leveraging Digital Tools for Effective Implementation", Panama, Octubre 2023
- "Preparando para el futuro: Regulaciones, Iniciativas Climáticas y el Mercado de Carbono", Panama, Septiembre 2023
- Varias presentaciones sobre CORBA y systema distribuido, Corea

## Affiliation to Organizations
- Project Management Professional (PMI), PMP Certified (2009)
- Member of The Indus Entrepreneurs (TiE)
