---
layout: layout/page_with_posts.njk
title: Young Suk Ahn Park

pagination:
  data: collections.post
  size: 3
  reverse: true
  alias: posts
---

<img src="/images/ysahn-202408.jpg" class="border-8 border-gray-200">

Soy promotor de la sostenibilidad, aprendiz, emprendedor, educador y arquitecto de software.

Actualmente me desempeño como CTO de [HanaLoop Corp](https://www.hanaloop.com), una empresa que cofundé junto con Hyeyeon Kim MS. En HanaLoop, estamos constantement mejorando nuestra innovadora plataforma de sostenibilidad diseñada para apoyar tanto a entidades privadas como gubernamentales en la gestión de gases de efecto invernadero (GEI). Nuestro objetivo es reducir las emisiones y contribuir a la mitigación de la crisis climática.

Mi historial profesional es diverso y abarca varias áreas. Tengo experiencia como consultor de informatica en Corea del Sur, Panamá y Costa Rica, así como en emprendimiento en el campo de la tecnología de la información. Además, tengo experiencia de haber creado una fundación sin fines de lucro para promover el arte y la cultura, donde he organizado conciertos y exposiciones artísticas.

En el pasado, he creado sistemas de software que ha impactado positivamente la vida de más de 200 millones de personas. También he mejorado drásticamente el rendimiento de mis equipos, ingenieros y propietarios de productos. A través de la mentoría y coaching, los desafié a construir un mejor producto, centrándose en la excelencia técnica y en entregar valor al cliente.

También cuento con experiencia como guía de turismo, tutor de matemáticas y programación, e investigador en una institución académica. Todas estas experiencias me han ayudado a desarrollar habilidades únicas para interpretar problemas y afrontar retos de manera efectiva.

En los últimos aňos, como promotor de la sostenibilidad, he impartido conferencias en Panamá, Brasil y Nepal, así como en varios simposios en Corea.

Fuera del ámbito laboral, disfruto pasar tiempo con mi familia, leer libros y producir videos. También me apasiona compartir conocimientos con los demás.



## Activiades que me mantiene ocupado:
* Difundir sostenibilidad en [HanaLoop.com](https://hanaLoop.com/), la empresa que co-funde.
* Desarrollar [hana.eco](https://www.hana.eco/), plataforma digital de gestion de carbono.
* Mantener [CreaSoft.dev](https://creasoft.dev/), compartir conocimientos de desarrollo y programación.
