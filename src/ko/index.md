---
layout: layout/page_with_posts.njk
description: HanaLoop 하나루프 공동대표 안영석, 하나루프 CTO 안영석, 탄소관리 플랫폼 하나에코 아키텍트, 지속가능성 탄소 중립 플랫폼 아키텍트, 협업형 탄소관리 플랫폼 아키텍트, Hana.eco 아키텍트
keywords: HanaLoop, 하나루프, CTO, 탄소관리 플랫폼, 지속가능성 플랫폼, 하나에코 아키텍트, Hana.eco 아키텍트, 소프트웨어 엔지니어, 지속가능성, 탄소중립, Net Zero 

date: 2021-04-19

pagination:
  data: collections.posts_ko
  size: 3
  reverse: true
  alias: posts
---

<img src="/images/ysahn-202408.jpg" class="border-8 border-gray-200" alt="Young Suk Ahn Park" >
<div class="text-gray-500 text-sm text-center align-middle">[2024]</div>


당연하게 여기던 것들에 질문을 던지며, 어떻게하면 다음 세대들에게 살만한 세상을 물려줄 수 있을까를 고민하고 있는 사업가이자 소프트웨어 아키텍트입니다.

소프트웨어 공학 전공으로 한국, 파나마, 미국에서 20년 이상 소프트웨어 시스템 구축에 관련된 일을 해왔습니다. 한국에서는 7년간 분산 시스템 컨설턴트로 여러 대규모 프로젝트에 참여했었습니다. 파나마에서는 5년동안 컨설팅 회사를 운영하며 파나마 정부 및 기업의 시스템 구축사업을 주도했습니다. 미국 Carnegie Mellon University에서 소프트웨어 공학 석사학위를 받았고, 다수 IT 회사에서 소프트웨어 아키텍트로 다양한 규모의 프로젝트를 이끌었으며, 현재 [(주)하나루프](https://hanaloop.com)를 파트너와 창업하여 CTO로써 SaaS형 [지속가능성 (탄소관리) 플랫폼 하나에코](https://www.hana.eco) 구축을 총괄하고 있습니다.  

<p></p>
<hr/>

## 어떻게 도와드릴까요?

<div class="flex"><img src="/images/icon-network.png" class="border-solid" style="height:30px; margin-right:.5em"> <h3>기업과 기관을 위한 디지털 지속가능성 솔루션 제공</h3></div>

- **기후규제에 원활히 대응**하기 위한 탄소관리 솔루션을 제공해 드립니다. 체계적으로 탄소를 관리하며, 탄소배출 핫스팟을 파앗하고, 탄소감축 전략을 수립하며 감축시나리오별 경제성 분석으로 기후리스크를 탄력적으로 관리할 수 있도록 해드립니다.
- **국내 배출권거래제 / 목표관리제를 비롯하여, EU 탄소국경조정제도**(EU CBAM, Carbon Border Adjustment Mechanism), IFRS S1,S2 등의 의무 공시, 보고에 원활히, 체계적으로 대응할 수 있도록 솔루션을 제공해 드립니다.
- **환경데이터 관리 솔루션**을 통해 환경데이터를 유기적으로 관리하고 분석함으로써 기업의 지속가능한 경영을 위해 유의미한 데이터를 제공해 드립니다.
- **지속가능경영보고서**를 위한 데이터를 체계적으로 관리하고 제공해드립니다.
 

<div class="flex"><img src="/images/icon-ecological.png" style="height:30px; margin-right:.5em"> <h3>기업 및 기관 지원</h3></div>

- **디지털 전환** (Digital Transformation)을 위한 컨설팅 서비스를 제공합니다.
- 기후변화로 인한 위험요소를 IT 기술과 데이터로 대응할 수 있도록 해드립니다.


<div class="flex"><img src="/images/icon-network.png" class="border-solid" style="height:30px; margin-right:.5em"> <h3>소프트웨어 개발팀 멘토링</h3></div>

- **애자일 개념과 소프트웨어 개발 ‘best practice’** 를 도입하여 팀의 퍼포먼스를 높여드립니다.
- **IT 시스템 아키텍트 및 프로세스를 최적화**해 드립니다.


<p></p>
<hr/>

## 저에 대해 더 궁금하시면
- [포트폴리오 슬라이드](/files/ysahn-cs-portfolio-20210428_en.pdf)
- [소개 페이지](/ko/pages/about_me/)
- [linkedin.com/in/ysahn](https://www.linkedin.com/in/ysahn/)
- [facebook.com/ysahn](https://www.facebook.com/ysahn)
- [기발가족 (YouTube)](https://www.youtube.com/channel/UCi6sTo5f7_FQDW8cvcZk1VA) - 우리 가족의 **기쁨과 발전** 이야기


<hr/>

## 고객
<div class="flex">
  <img src="/images/cust/cust-bac-logo.png" alt="PanamaCompra" class="m-3 h-8" >
  <img src="/images/cust/cust-bbva.png" alt="PanamaCompra" class="m-3 h-8" >
  <img src="/images/cust/cust-lg_elec.png" alt="PanamaCompra" class="m-3 h-8" >
  <img src="/images/cust/cust-asep.jpg" alt="ASEP" class="m-3 h-8" >
  <img src="/images/cust/cust-panamacompra.gif" alt="PanamaCompra" class="m-3 h-8" >
</div>

<hr />
<div class="mt-4 text-gray-500 text-sm ">Icons made by <a href="https://creativemarket.com/Becris" title="Becris">Becris</a>, <a href="https://icon54.com/" title="Pixel perfect">Pixel perfect</a>, <a href="https://www.freepik.com" title="Freepik">Freepik</a>, <a href="https://www.flaticon.com/authors/iconixar" title="iconixar">iconixar</a> from <a href="https://www.flaticon.com/" title="Flaticon">www.flaticon.com</a></div>
