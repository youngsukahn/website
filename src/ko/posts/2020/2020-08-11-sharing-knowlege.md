---
title: 지식 공유
date: 2020-08-11
tags: ["post"]
---

지식 공유하고자 유투브를 시작하였습니다: [EmpoderemosMas.com](https://empoderemosmas.com/)  그리고  [기발가족](https://creasoft.dev/).

## 왜?
무엇을 시작하기전 '왜'라는 질문 부터 시작해야죠.

### The answer is straight forward: To share knowledge

So, **why I share my knowledge?**

#### First, to learn

Not to show off that I am knowledgeable, in the contrary, because my knowledge is limited and
those things I know, the level of understanding is shallow. The process of sharing knowledge
starts with internalizing the topic so that I can articulate for the audience to understand.

As organize my thought I find gaps in my knowledge, motivating me to research further. Once I
am done writing an article, I find myself with a bit more mastery in the  topic and a bit better communicator.

#### Second, to contribute

Even with my limited knowledge, I believe it is worth sharing. If it has been useful to me,
it could be useful to someone else.

#### Third, because it is fun

Learning is fun, sharing is fun. The actual mechanics writing, not so much, but that's where I am going beyond my comfort zone.
