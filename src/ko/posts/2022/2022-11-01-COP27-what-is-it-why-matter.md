---
title: "COP27이 뭐예요? 왜 중요한가요?"
description: "COP27에서 다룰 핵심 내용: 1. 완화. 국가들이 배출량을 줄이는 방법, 2. 적응. 각 나라들은 어떻게 적응할 것인가, 3. 자금조달. “손실과 피해”"
keywords: COP27, climate crisis
author: YS
date: 2022-11-17
tags: ["post"]
---

## COP이 무엇인가?

현재 세계적으로 일어나고 있는 지정학적 긴장, 경기 침체의 위험, 에너지 위기 및 기록적인 기후재해 속에서 COP27은 기후행동의 분수령이 될 것으로 많은 이들이 인식하고 있습니다.

기후변화를 막기 위한 액션은 전 지구적으로 같이 해야지!라고 생각하고 있는 당신. 그러니 글로벌 리더들이 주기적으로 모여 기후변화 대응에 힘써야 한다고 생각한다면, COP가 바로 그것입니다.

COP는 "Conference of the Parties, 당사국 총회"의 약자로 매년 열리는 유엔 기후 정상회의입니다. 이 회의는 1994년부터 매년 개최되었고, 지구 기후 목표를 논의하고 협상해 왔습니다. 2015년 제21차 당사국총회(COP21, 파리)에서 채택된 파리협정(Paris Agreement)은 기후변화 대응을 위한 중요한 이정표였습니다.

2022년 COP27은 11월 6일부터 18일까지 이집트 샤름 엘 셰이크에서 개최됩니다. 세계에서 기후변화에 가장 취약한 지역 중 한 곳인 아프리카에 관심을 집중시키려는 의도가 장소 설정에 담겨있습니다.

최근에 발생한 역사적 가뭄과 기록적인 홍수가 지도자들이 협력하고 구체적인 결과를 도출하고 신속하게 실행에 옮기는 데 구동력이 되기를 바랍니다.

COP27은 '손실과 피해'에 대한 의제를 설정하면서 마찰로 시작되었습니다. 이 갈등은 대화가 얼마나 논쟁의 여지가 있는지에 대한 경고 시그널이기도 합니다.


“오늘 새로운 시대가 시작됩니다. 우리는 다르게 행동하기 시작합니다. 
Paris에서 우리는 합의했습니다. 
Katowice와 Glasgow에서 계획했습니다.
Sharmel-Sheik에서 우리는 구현으로 전환합니다.
아무도 이 여정에 그저 나그네일 수 없습니다.
이것은 시대가 바뀌었다는 신호입니다.”
UNFCCC 신임 사무총장 사이먼 스틸


## COP26의 요약

COP27은 Glasgow의 [COP26 결과](https://www.un.org/en/climatechange/cop26)를 바탕으로 합니다. COP26 결과를 간략히 요약해볼까요?
 
- 모든 국가에게 더 야심찬 계획을 요구
- 당사자들에게 석탄 소비의 단계적 감소 가속화를 요청
- 선진국들의 1,000억 달러 목표를 시급히 완수할 것을 재확인 (COP15에서 피해 국가에 연간 1,000억 달러 지원하는 목표를 세웠었음) 
- 파리 규정집 완성
- “손실과 피해”의 중요성을 일깨움
또한 재조림, 메탄 감축, 넷제로를 향한 재정 목표변경에 대한 합의와 약속이 있었습니다.

해결되지 않은 채로 남아있는 중요한 주제 중에는 다음과 같은 내용들이 있습니다:

- 손실과 피해 재무에 대한 구체적인 내용
- 글로벌 탄소 시장  구축 - 글로벌 시장에서 탄소 가격 책정
- 석탄 사용을 줄이기 위한 공약(commitments) 강화

실망스러웠던 부분은 석탄 화력 발전의 “단계적 폐지”에서 “단계적 축소”로 마지막 순간에 문구 변경이 있었다는 것입니다. 

## COP27에서 다룰 핵심 내용

정상회의의 핵심은 국가들이 기후 목표를 어떻게 달성하고 자금 조달 측면을 구체화할지를 파악하는 것입니다. 

아래 3가지 테마를 핵심으로 꼽을 수 있습니다.

### 1. 완화. 국가들이 배출량을 줄이는 방법
국가 감축 목표를 파리협정 수준으로 맞추는 것이 이번 회담의 주요 사업입니다.

각 나라들은 감축 공약과 진행 상황, 그리고 그들의 NDC가 얼마나 도전적인가, 보고서는 얼마나 투명한가, 배출을 실제로 얼마나 줄였는가 하는 질문들을 바탕으로 논의할 것입니다.


이전 COP에서 Glasgow Climate Pact는 국가들이 보다 강화된 목표로 목표를 수정할 것을 촉구했습니다. 현재 공약은 상승기온 1.5c를 유지하는 데 부족합니다. 세계는 세기말까지 2.5°C의 온난화를 볼 가능성이 있습니다. 그런데 정상회의가 시작된 시점에서 193개국 중 29개국만이 보다 야심 찬 계획을 이행했습니다.

[IMF의 보고서](https://climatedata.imf.org/pages/re-indicators)는 전세계 배출량이 팬데믹 기간 동안 잠시 감소한 후 지속적으로 증가하는 추세를 보여줍니다. 이전의 조치가 배출량을 억제하기에 충분하지 않았다는 증거입니다. 

### 2. 적응. 각 나라들은 어떻게 적응할 것인가
작년에 기후 재해로 인해 세계 경제가 약 3,290억 달러에 달하는 피해를 입었습니다[취리히](https://www.zurich.com/en/knowledge/topics/climate-change/adapting-to-change-building-resilience-to-extreme-weather). 파괴적인 극한 날씨가 더 큰 규모로 더 빈번해질 것으로 예상됩니다. 따라서 배출을 줄이는 동시에 기후 복원력에 투자하는 것이 필수가 되었습니다.

파키스탄을 생각해 봅시다. 이 나라는 최근에 대홍수로 약 400억 달러에 달하는 손실을 입었습니다. 적절한 기반 시설이 없으면 그 나라는 기후 복구 루프에서 벗어나지 못할 것입니다.

2009년 코펜하겐에서 열린 COP 15에서 선진국들은 저소득 국가의 기후 위기 적응을 지원하기 위해 2020년까지 연간 1,000억 달러를 투입하기로 합의했습니다. 그러나 그 약속은 아직 지켜지지 않았습니다.

적응에 대한 정의와 프레임워크는 완화 목표보다 발전이 미흡하지만, COP27에서 더 자세히 논의되기를 바랍니다.

### 3. 자금조달. “손실과 피해”
완화 또는 적응으로 피할 수 없는 피해가 발생한 경우, “손실과 피해” 자금조달을 통해 해결합니다. 이 주제는 COP26 이후에도 결론을 내리지 못한 채 남아있었고, 정상회의 시작 시 의제에 대한 의견 불일치로 인해 대화에서 많은 장애물이 있을 것으로 예상할 수 있습니다.

“손실과 피해”의 원리(rationale)는 온실가스를 많이 배출하는 선진국은 기후변화로 심각한 영향을 받는 저소득 국가에 대가를 지불해야한다는 것입니다. 아이러니한 점은 저소득 국가들은 역사적으로 배출량이 적음에도 불구하고 기후에 가장 취약하는 것입니다.

COP26에서의 투발루 장관 사이먼 코페(Simon Kofe)의 연설[YouTube](https://www.youtube.com/watch?v=jBBsv0QyscE)은 기후변화가 작은 섬 국가들에 미치는 영향을 충격적으로 전하고 있습니다.    


“이것은 기후 정의, 국제적 연대 및 신뢰의 근본적인 문제입니다.”
안토니오 구테레스, 유엔 사무총장


## COP27은 

과학적 연구는 기후 변화의 심각성을 재확인했습니다[IPCC](https://www.ipcc.ch/report/ar6/wg1/), [WMO](https://mcusercontent.com/e35fa2254c2a4394f75d43308/files/3d2f5b01-11a6-dee2-a42a-13ad5002dc33/1290_Statement_2021_en_1_.pdf). 많은 국가들이 이미 극단적인 날씨에 높은 대가를 치르고 있습니다.  

중요하며 위험은 높지만 현재 설정은 어렵습니다. 전쟁은 에너지 위기를 악화시켰고 미중 경쟁은 글로벌 협력을 저해하고 한반도의 긴장 고조는 이미 복잡한 문제에 복잡성을 더하고 있습니다.

우리는 중요한 순간에 있습니다. 국가들이 공동의 목표에 대해 합의하고 힘을 합치지 못한다면 세계는 티핑 포인트에 도달하여 부정적인 스파이럴 효과가 발생할 것입니다.

COP27은 반드시 규범으로 이어질 글로벌 합의와 거래로 이어질 것입니다. 한편, 기관들은 규정이 확정될 때까지 기다리면 안됩니다. 기업은 계획을 세우고 실행에 옮겨야 합니다.

결국 우리는 행동이 필요하며, 실제로 실행할 때 COP가 성공적이었다 이야기할 수 있을 것입니다. 


# 참고
[State of the Global Climate 2021: WMO](https://mcusercontent.com/e35fa2254c2a4394f75d43308/files/3d2f5b01-11a6-dee2-a42a-13ad5002dc33/1290_Statement_2021_en_1_.pdf)

[What are the key issues at COP27?, Chathamhouse](https://www.chathamhouse.org/2022/08/what-cop27)

[COP27: What you need to know about this year’s big UN Climate Conference, UN](https://news.un.org/en/story/2022/10/1129947)

[COP27: Your Guide to the 2022 UN Climate Conference, The Nature Conservancy]()https://www.nature.org/en-us/what-we-do/our-priorities/tackle-climate-change/climate-change-stories/cop-climate-change-conference/

[COP26 Outcomes: Market mechanisms and non-market approaches (Article 6), UNFCC](https://unfccc.int/process-and-meetings/the-paris-agreement/the-glasgow-climate-pact/cop26-outcomes-market-mechanisms-and-non-market-approaches-article-6)

[COP26: Together for our planet, United Nations](https://www.un.org/en/climatechange/cop26)

[COP26 Outcomes, Transparency and reporting, UNFCC](https://unfccc.int/process-and-meetings/the-paris-agreement/the-glasgow-climate-pact/cop26-outcomes-transparency-and-reporting) 

[What is COP27 and why is it important?, BBC](https://www.bbc.com/news/science-environment-63316362) 

[Report of the Conference of the Parties serving as the meeting of the Parties to the Paris Agreement on its third session, held in Glasgow from 31 October to 13 November 2021, UNFCC](https://cop23.unfccc.int/sites/default/files/resource/cma2021_10a01E.pdf) 

[Greenhouse Emissions Rise to Record, Erasing Drop During Pandemic, IMF Blog](https://www.imf.org/en/Blogs/Articles/2022/06/30/greenhouse-emissions-rise-to-record-erasing-drop-during-pandemic)

[Timeline: The struggle over ‘loss and damage’ in UN climate talks, CarbonBrief](https://interactive.carbonbrief.org/timeline-the-struggle-over-loss-and-damage-in-un-climate-talks/)





이글은 [하나루프의 공식 브런치](https://brunch.co.kr/@hanaloop/)에서도 보실 수 있습니다. 
