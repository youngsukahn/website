---
title: 안영석에 대해 좀 더 자세히 
description: HanaLoop 하나루프 공동대표, 하나루프 CTO 안영석, 탄소관리 플랫폼 아키텍트, 지속가능성 플랫폼 하나에코 아키텍트, Hana.eco 아키텍트
keywords: HanaLoop, 하나루프, CTO, 탄소관리 플랫폼 아키텍트, 지속가능성 플랫폼 아키텍트, 탄소중립 플랫폼 아키텍트, 하나에코, Hana.eco 아키텍트, 소프트웨어 엔지니어, 지속가능성, 탄소중립, Net Zero 
date: 2021-04-19
slug: /more-about-me
---

<img src="/images/20210101_family.jpg" alt="picture" class="border-solid">
<div class="text-gray-500 text-sm text-center align-middle">[코비드 때 가족과 함께 산책, 2021]</div>

피조물 하나 하나에 귀를 기울이니 지혜의 소리가 들립니다.  
한가지를 배우고 나면 두가지를 모르고 있다는 것을 깨닫게 됩니다.  
배우고, 실행하고, 실패하고, 되돌아보고, 공유하고, 또 다시 배우는 과정이 흥미롭습니다.

인간의 영향으로 악화되고 있는 소중한 지구를 보며 환경 보호와 지속가능성의 중요성을 의식하고 있습니다.

안녕하세요, 저는 아름다운 가족을 축복으로 받은 사람입니다. 한국에서 태어났지만 어렸을 때 파나마에 이민가서 자랐기에 한국과 라틴 문화가 나의 일부가 되었습니다. 파나마와 한국 간 문화교류 및 육성을 위해 Culturaplus라는 비영리기관을 설립해서 파나마 예술가들을 홍보하고 한국 문화도 파나마에 소개하는 프로젝트들을 추진했습니다. 경제적으로 성공한 것도 있고 실패한 것도 있지만 궁극적으로 나의 성장과정에서는 모든 경험들이 이득이었다고 봅니다. 

회사 설립 경험도 있습니다. 소규모 컨설팅 비즈니스였지만 회사 운영과 고객관리를 터득하게 해준 휼륭한 배움터였습니다. 처음으로 창업했던 회사는 Carnegie Mellon University에서 Software Engineering에 대해 좀더 집중적으로 공부하며 경험을 쌓기 위해 5년만에 문을 닫으며 마무리하였습니다. 이번에 다시 시도하는 사업은 클라이밋 테크 스타트업 [하나루프](https://hanaloop.com)입니다. 시급한 기후위기문제를 해결하는데 가진 역량을 최대한 발휘하자는 취지로 시작한 사업입니다. 

저는 현상 유지에 만족하지 않고 꾸준히 개선을 추구합니다. 무엇이 필요하고, 무엇을 바꿔야 하는지를 파악하고 변화를 위해 실천합니다.

현재 저는 나의 시간 대부분을 [탄소관리 플랫폼 (지속가능성 플랫폼)](https://ecoloop.io)을 구축하는데 투자하며 [탄소중립 가속화](https://hanaloop.com/docs/x20_environment-general/2022-10-12-net-zero)를 위해 노력하고 있습니다. 저는 이 [기후위기 해결을 선도할 있는 나라는 한국](https://www.youtube.com/watch?v=twZeB5s-9ls)이라고 믿고 있습니다. 그렇게 믿는 이유가 궁금하시면 [여기 클릭!](https://www.youtube.com/watch?v=twZeB5s-9ls)  

여가시간에 경제, 철학, 심리학, 역사, 환경 등 다양한 분야를 공부하며 배움의 기쁨을 느끼고 있습니다. 학창시절에는 관심이 없었던 분야들인데 사회 경험을 한 지금은 다시 보니 매우 재미 있네요. 배운 것을 정리해 비디오로 만들어 가족과 친구, 이 주제에 관심있는 사람들과 공유하고 있습니다. 유튜브 채널: 
[한국어](https://www.youtube.com/channel/UCi6sTo5f7_FQDW8cvcZk1VA), [스페인어](https://www.youtube.com/channel/UCW3h5kacfHTE74Ta2WScMMw), [영어](https://www.youtube.com/channel/UCH5ychAx_0uS8zTr7_07_wQ).


10년간 미국 중부, 동부, 서부에서 직장생활을 하면서 다양한 북미 문화를 접할 수 있었던 것을 감사하게 생각합니다. 


## 나의 가치관

나의 가치관들은 나의 삶의 지침입니다.
See also: [Lifecraft Radar](/en/lifecraft-radar/).

- **믿음** - 나를 지지하는 무한하게 현명한 분을 믿는다. 그리고 모든 관계를 맺을 때 상대방을 믿는다는 것을 전제로 한다. 
- **존중** - 사람은 모두 존중하자. 사람의 외모 및 일시적인 행동으로 쉽게 판단하지 않도록 노력하자. 
- **성장** - 당장의 편리함과 안전보다 불편하더라도 성장하게 하는 것을 선호하자.

딸이 7살 때 철봉을 잡으려고 플랫폼 위에서 뛰는 것을 본 적이 있습니다. 끈기가 있는 딸은 떨어지는데도 10번 이상 뛰고 있었습니다. 그런데, 계속 철봉을 잡지 못하는 딸아이의 시도를 유심히 보니 딸은 뛰기 전에 철봉을 한번 보고 정작 뛸 때는 떨어질 바닥을 보고 뛰고 있었습니다. 그래서 저는 아이에게 다가가 내가 떨어지면 잡아줄 테니 힘껏 뛰라고 하였습니다. 아빠가 받쳐줄 것을 믿은 딸은 철봉을 보고 뛰었고 바로 잡을 수 있었습니다. 딸 아이와의 경험을 통해 **믿음은 우리에게 큰 힘이 된다**는 것을 깨달았습니다.

성장은 혼자 하는 것이 아니라 함께 하는 것이라고 생각합니다. 그래서 배운 것들을 정리하여 YouTube에 올리고 있습니다. 그리고 나의 소스 코드는 모두 오픈 소스이며 GitLab 혹은 GitHub에 개방되어 있습니다. 제가 공유하는 비디오와 소스코드들이 서로의 성장에 도움이 되기를 바랍니다.  

##  개발

- [github.com/ysahnpark](github.com/ysahnpark/)
- [gitlab.com/ysahnpark](github.com/ysahnpark/)
- [하나에코, EcoLoop Dev Portal](https://hanaloop.gitlab.io/pp/ecoloop-devportal/) - Digital Transformation for Sustainability, 하나에코, EcoLoop, a platform for climate insight


## SNS

- [linkedin.com/in/ysahn](https://www.linkedin.com/in/ysahn/)
- [facebook.com/ysahn](https://www.facebook.com/ysahn)
- [twitter.com/pluspective](https://twitter.com/pluspective)
- [medium.com/@pluspective](https://medium.com/@pluspective)
- [하나루프 공식 브런치, https://brunch.co.kr/@hanaloop](https://brunch.co.kr/@hanaloop)


## 컴퓨터 외 활동

- 문화 교류 및 육성을 위한 비영리기관  Culturaplus 설립. 아트 공연, 음악 콘서트 기획, 신진 예술가들 지원
- 어린이 수학 교사
- 어린이 프로그래밍 교사
- 어린이 주일학교 교사

## Products

* [탄소관리 플랫폼(지속가능성 플랫폼) 하나에코](https://www.hana.eco) - 기업활동 중 배출하는 모든 온실가스 활동(탄소 활동) 데이터를 수집, 배출량 산정, 데이터 분석, 전략 수립 및 이행을 지원하는 탄소중립 플랫폼  
* [기발가족 (YouTube)](https://www.youtube.com/channel/UCi6sTo5f7_FQDW8cvcZk1VA) - 우리 가족의 **기쁨과 발전** 이야기
* [CreaSoft.dev](https://creasoft.dev/), [Fundamenty](https://fundamenty.netlify.app/)
* Empoderemos Mas: [EmpoderemosMas](https://empoderemosmas.com/),  [Twitter](https://twitter.com/empoderemosmas), [YouTube](https://www.youtube.com/channel/UCW3h5kacfHTE74Ta2WScMMw/)

## 현재 관심분야

- **친환경, 지속가능 기술** - 환경이 너무 파손되었어요! 회복하는데 각 분야에서 모두의 참여가 요구되는 현실입니다.
- **철학** - 위키피디아 링크를 계속 따라가다보면 철학적 콘텐츠에 도달하게 됩니다. 인간은 깊은 질문을 꾸준히 하면서 살아야 제대로 사는 것이라고 생각합니다.
- **심리학** - 사람들과 함께 일할 때 큰 도움이 됩니다.
- **생체 모방 (Biomimicry)** - 수십억 년의 지혜가 자연 속에 있습니다.
- **스타트업 및 기업가 정신** - 신선한 아이디어와 도전정신으로 더 많은 창업이 필요합니다.
- **블록체인** - 현재 금융 시스템은 단점이 많습니다. 블록체인과 같은 기술이 보완되면 보다 포괄적인 금융 시스템을 구축할 수 있을 것 같습니다.
- **Rust** - 퍼포먼스와 안전한 메모리 관리를 함께 제공하는 개발 언어. 

## 그리고 나는 

- **시**를 썼었어요. 그런데 요즘은 시 쓸 영감이 안떠오르네요.
- 12개국을 **여행**했어요. 여행은 다양한 문화를 경험하는데 최고죠.
- **아트 딜러** 했었어요. KIAF2007에서 대가들 사이에서 파나마 아트를 전시한 소중한 경험이 있습니다. 
- 음악 **콘서트 기획**을 했었어요. 오페라 콘서트 2번, 재즈 콘서트 2번.
- **여행 가이드**를 했었어요. 한국 투어회사와 협력하여 한국 관광객들에게 파나마 투어를 해드렸습니다.
- **비디오 제작**하여 유튜브에 올리고 있어요. 

## 나의 향후계획

- 더 많은 나라 방문, 문화 체험
- 산티아고의 길(Camino de Santiago de Compostela) 순례
- 물건 공유앱 개발
- achievely.com 개발

## 내가 사용하는 서비스 및 도구들

### 소프트웨어 개발 도구
- GitLab (버전 관리 서비스) -  GitHub Action 전에 GitLab-CI에 익숙해졌습니다. GitLab의 계층적 폴더 구조가 GitHub의 프로젝트 구조보다 repo 관리하는데 좀 더 직관적이네요. 이 사이트도 GitLab에 무료로 호스팅하고 있습니다.
- GitHub (버전 관리 서비스)
- IntelliJ (코드 에디터)
- VS.code (코드 에디터)
- Fundamenty (문서)
- Docker (컨테이너)
- Playwright (브라우저 자동화)
- Jest (JS 테스팅 프레임워크)
- Snyk (보안)
- Auth0 (인증)
- ElasticSearch (검색)
- SpringFramework (애플리케이션 개발 프레임워크) 

### 유용하게 사용하는 서비스
- Google Docs
- Zoom
- MS Teams
- Kakao
- LinkedIn §
- Facebook §
- YouTube §
- Medium
- brunch 

§ 빠져들어 시간 낭비 안될 만큼 시간 조절 하면서. 

<img src="/images/20201024_thankyou.jpg" alt="picture" class="shadow" style="width: 200px">
