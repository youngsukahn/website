---
title: 하나루프 탄소관리 플랫폼 하나에코, 에코루프 CTO 안영석 (Ahn Park Young Suk) 이력서 Resume
description: 소프트웨어 아키텍트 안영석 이력서; Software Engineer Ahn Park Young Suk Resume; 하나루프 탄소관리 플랫폼, 지속가능성 플랫폼 아키텍트 안영석 이력서
keywords: HanaLoop 하나루프 공동대표, 하나루프 CTO, Hana.eco, EcoLoop 아키텍트, 기술의 임팩트에 가치를 두는 개발자, 온실가스 관리 플랫폼 구축, 탄소관리 플랫폼 구축, 하나에코, SaaS형 공급망 포함 탄소관리 플랫폼 에코루프 구축, 지속가능성 플랫폼 에코루프 구축
date: 2022-10-05
slug: /resume
---

## 소개
> 기후 변화가 주변에 미치는 부정적인 영향을 보며 더 이상 무관심할 수 없다는 것을 깨달았습니다. 상황의 긴박함을 느껴, 2021년에 소프트웨어 아키텍트로서의 안정된 직장을 떠나 기술을 통해 기후 변화에 맞서는 여정을 시작했습니다.


한국, 파나마, 미국에서 20년간 소프트웨어를 만드는 사업에 종사했습니다. 한국에서는 분산시스템 컨설턴트로 여러 대규모 프로젝트에 참여했었습니다. 파나마에서는 컨설팅 회사를 운영하며 파나마 정부 및 민간기업들의 시스템 구축사업을 주도했습니다. 최근까지 미국 월트 디즈니 스튜디오에서 애자일(Agile)한 프로젝트 환경을 만들어가며, 소프트웨어 플랫폼을 구축했습니다. 2021년 한국에서 환경을 위해 클라이밋 테크 스타트업 [(주)하나루프](https://hanaloop.com)를 공동창업하였습니다. 기업의 **공급망 포함 탄소관리 플랫폼**, [하나에코(hana.eco)](https://www.hana.eco/)를 통해 데이터 기반 지속가능성 생태계 형성을 가속화하려고 합니다.

저의 또 다른 열정은 **홍익인간정신 실천**에 있습니다. 그것은 인류의 공동선을 지향하며 기술과 문화의 힘을 나눌 때 가능하다고 생각합니다. 파나마에서는 문화교류 및 육성을 위해 비영리기관을 설립하여 파나마 예술가들을 홍보하고 한국 문화도 파나마에 소개하는 프로젝트들을 추진했습니다. 미국에서는 지역 초등학교 어린이들에게 프로그래밍을 가르쳤습니다. 코로나 이후 지식 공유의 임팩트를 최대화하기 위해 유용한 트렌드 기술 지식을 [유튜브](https://www.youtube.com/@empoderemosmas6178) 채널을 통해 전였습니다. 

미디어 및 엔터테인먼트, 교육, 통신, 금융, 교통, 유통, 예술, 바이오인포메틱스 등 다양한 분야에서 연구, 개발부터 프로젝트 아키텍팅, 팀 리드, 창업을 통해 도메인 경계에 국한되지 않는 융통성있는 문제 해결능력과 추진력을 키울 수 있었습니다.
**제가 만드는 소프트웨어가 사람들의 삶을 더욱 풍요롭고 아름답게 하는 것이 저의 미션**입니다.

현재 기후변화 위기로 전지구적으로 대책이 시급합니다. 기후변화의 주범인 온실가스 배출의 70%를 차지하는 **산업이 보다 지속가능한 운영으로 기후변화 위기에 대응할 수 있도록 지원하는 것**이 **인류와 인류문명의 근간인 산업과 지구의 공생을 위한 길**이라고 생각합니다. 기업의 지속가능성을 데이터로 지원하기 위한 **SaaS형 지속가능성 플랫폼, 온실가스 관리 플랫폼, 하나에코**를 만들고 있는 이유입니다. 


## 교육
- 2010/10 ~ 2011/12, Carnegie Mellon University, 미국, 소프트웨어 공학, 석사
- 1995/09 ~ 1997/09, 고려대학교, 한국, 컴퓨터과학, 학사
- 1994/01 ~ 1995/09, Florida State University, Panama Canal Branch, 파나마, 전산학, 학사

## 언어
한국어, 영어, 스페인어


## 기술
- 방법론: Agile(SCRUM, XP, Kanban), 소프트웨어 아키텍팅, Domain Driven Development(DDD)
- 개발언어: Java, Kotlin, JavaScript/Typescript, Python, PHP, C#, C/C++
- 프레임워크 및 플랫폼: Spring Framework, Express, Nextjs, Nestjs, Angular, React, Laravel


## 경력
- CTO, co-founder. **(주)[하나루프 / HanaLoop Corp](https://www.hanaloop.com/)**, 서울, 한국 (2021/09/01 ~ 현재)

    [SaaS형 공급망 포함 탄소관리 플랫폼 - 하나에코, 에코루프](https://www.hana.eco) 구축, 기업 전략 컨설팅.

- 수석 아키텍트. **The Walt Disney Studios**, 버뱅크, 미국 (2016/10/17 ~ 2021/12/27)

    미디어 배포 플랫폼 구축. DevOps 플랫폼 도입. 아키텍트 코칭. 엔지니어 교육 및 육성을 통한 테크 커뮤니티 활성화. 

- 시니어 아키텍트. **Pearson**, 보스톤, 미국 (2013/06/26 ~2016/09/30)

    온라인 인터랙티브 교육 컨텐츠 스펙 정의 및 프레임워크 구축. 차세대 교육 시스템 통합. 

- 소프트웨어 엔지니어. **Wayfair LLC**. 보스톤, 미국 (2013/01/21 ~ 2013/06/19)

    웹과 모바일 전자상거래 프레임워크 구축. 

- 연구원. **Pittsburgh Science of Learning Center**, Carnegie Mellon University, 피츠버그, 미국 (2012/01/09 ~ 2013/01/10 Part Time)

    학습데이터 분석시스템 퍼포먼스 개선(DataShop)

- 연구원. **Institute for Software Research (ISR)**, Carnegie Mellon University, 피츠버그, 미국 (2012/01/09 ~ 2012/12/20 Part Time)

    CMU의 David Garlan 교수와 Task Oriented Computing 아키텍처 연구. 홈빌딩 자동화(Home Building Automation)에 적용.

- 연구원. **Dynamic Decision-Making Lab**, Carnegie Mellon University, 피츠버그, 미국 (2010/09/06 ~ 2011/12/20)

    Cyber Security Situational Awareness 연구. 아래 논문 참조.

- 창업자 / CTO. **Altenia Corporation**, 파나마 (2005/08/01 ~ 2010/07/30)

    EcoLogix™ ERP 시스템 구축 총괄 및 컨설팅. LG 라틴아메리카 콜센터, 파나마 이동통신사 감사시스템 구축, BBVA은행 서비스 버스 구축.

- 과장. **Nuri Solutions**, 한국 (2004/01/02 ~ 2005/05/31)

    금융IT 통합 솔루션 신사업 총괄.

- Technical Account Manager / 컨설턴트. **IONA Technologies**, 한국 (2001/04/01 ~ 2003/05/30)

    CORBA 제품 컨설팅 및 교육. 국내 파트너 및 주 고객 관리.

- 소프트웨어 엔지니어. **METHODi**, Seoul Korea (1999/02/01 ~ 2000/12/23)

    KT 네트워크 통신망, 신공항하이웨이 시스템 미들웨어 구축 등 CORBA 프로젝트 참여.

## 주요 참여 프로젝트
*   **수석 아키텍트,  디지털 미디어 배포 (Digital Media Distribution Center of Excellence), The Walt Disney Studio**, 버뱅크, 미국 (2017/05 ~ 2018/12) 

    해마다 십여개의 블록버스터 영화를 제작하는 디즈니는 Disney+ 스트리밍 서비스 출시 타이틀 확보를 위해 폭스사를 인수했습니다. 양사는 라이선스 관리 및 미디어 배포 프로세스를 각자 보유하고 있었으나 두 시스템 모두 많은 수작업을 필요로 했던 중 디즈니의 폭스사 인수 후 시스템 중복과 함께 운영팀도 중복되었습니다. 이러한 배경에서 디즈니는 Disney+외 다른 스트리밍 업체, 방송사, 호텔 등 디즈니 콘텐츠 유통 사업자 대상 타이틀 배포 프로세스의 효율성을 향상시키고 중복된 시스템을 병합하기 위해 Digital Media Distribution Center of Excellence 프로젝트를 착수했습니다. 프로젝트 세부목표:

    1. 디즈니 및 폭스사의 데이터와 시스템 병합
    2. 라이선스 관리를 개선하여 시장 확장 및 판매량 증가
    3. 유통 사업자별 맞춤 미디어 형상관리와 전달하는 절차 모니터링 및 자동화 

    수석 아키텍트로서 주요 역할: 
    *   미디어 배포 플랫폼에 적합한 아키텍처 설계. 주요 시스템 간의 인터페이스 설계 지도
    *   다수 엔지니어링 팀 리드. 애자일 방법론 도입. 기술 지침 제공 및 코드 검토 
    *   사용할 테크놀로지들을 선정하는 과정 감독
    *   현재 상태를 분석하고 이상적인 프로세스와 비교하여 차이 식별. 분석 자료를 바탕으로 비즈니스 리스크에 따라 시스템의 우선 순위 지정
    *   주요 이해관계자들과 정기적으로 요구 사항을 업데이트하고 검증 
    *   라이선스 관리 데이터 마이그레이션을 위한 ETL 서비스 개발. 라이선스 관리는 가장 복잡하면서 중대한 비즈니스임으로 개발에 직접 참여

    프로젝트를 시행하는데 있어 가장 큰 도전은 이해관계자들의 의견을 일치시키는 것이었습니다. 애초부터 다른 부서에 속해있던 약 60명 규모의 팀들간 우선순위를 정하는 것이 매우 어려웠으며 관리층의 의견 차이로 협업을 합의하는데 4개월간 각각 개인 업무시간의 20% 이상의 시간을 소요해야 했습니다. 기술적인 측면에서는 기존 시스템에 있던 복잡한 라이센스 데이터가 정확하지 않아서 연동 시스템에 전달하는 정보만 가지고는 자동화 작업이 가능하지 않았습니다. 팀간 상위레벨 아키텍처 설계 세션을 통해 팀간 마찰을 완화시키고 Contract-Driven Development를 채택하여 팀 종속성을 감소시켰습니다. 명확한 기술적 근거를 제공하여 관리층의 이견을 좁히는데 성공했습니다.

    이 프로젝트에서의 성과는

    *   관계자 전체가 이해할 수 있는 포괄적인 통합 아키텍처를 제공 
    *   부정확한 라이선스 데이터 마이그레이션을 고민하던 문제를 중간 단계 데이터베이스에서 정규화하고 자동화 검증 단계를 거치는 구조를 제안 및 구축 
    *   애자일 문화를 교육하고 도입하여 분산된 팀들의 협업 효율성을 높이고, 팀간 마찰을 감소시킴
    *   새로운 개발언어 Kotlin을 도입하여 개발 생산력을 증가시킴

    프로젝트 규모: 예산 2천4백만달러, 60여명 참여

    사용한 기술 및 기법: Scrum, Domain Driven Design, Test Driven Development, OpenAPI, VueJS, Kotlin,  SpringFramework, Postgresql

*   **아키텍트, 차세대 온라인 교육 시스템, Pearson**, 보스톤, 미국 (05/2015 ~ 09/2016) 

    170년 역사의 교육 출판사가 온라인 교육 서비스 공백을 채우기 위해 다양한 회사를 인수한 상태였습니다. 독립적으로 구현된 애플리케이션들은 기술적으로는 서로 상호연동이 잘 안되었고 오래된 기술을 사용하고 있었으며 기능적으로는 인터렉티브 학습기능이 부족했습니다. 그러한 연유로, 차세대 온라인 교육 시스템 프로젝트의 목적은 인터렉티브 학습 콘텐츠를 위한 기반을 제공하고 그와 관련된 모든 애플리케이션을 통합하는 것이었습니다.

    저는 인터렉티브 콘텐츠의 핵심 프레임워크를 만들고 애플리케이션 통합 계획을 수립하는 일을 담당했습니다. 구체적으로

    *   콘텐츠 전문가와 협력하여 요구 사항 분석
    *   IMS / QTI 표준을 기반으로 콘텐츠 스펙 정의
    *   다양한 학습 콘텐츠 유형을 위한 인터렉티브 콘텐츠 플레이어 (Content Player) 구축 (예: 다중 선택, 드래그 앤 드롭 평가, 비교 슬라이더 등)
    *   새로운 인터렉티브 콘텐츠 프레임워크 채택에 따른 다른 애플리케이션 개발 팀을 지원

    이 프로젝트의 가장 어려웠던 점은 요구 사항이 매우 추상적이었고 광범위한 학습 개념을 스펙 언어(Specification Language)에 담는 것이었습니다. 이렇게 새롭고 추상적인 영역에서 새로운 시스템을 개발할 때 애자일의 “고객과 협업” 철학이 중요하다는 것을 확인할 수 있었습니다.

    결국 JSON 기반으로 스펙 언어를 설계, 이 스펙을 렌더링하는 콘텐츠 플레이어를 개발하여 여러 대학교에 배포했습니다. 대학 교육 담당자들은 인터렉티브 콘텐츠를 통해 새로운 형태의 학습이 가능하다며 좋은 리뷰를 보내왔습니다.


    이 프로젝트에서의 성과는 

    *   통합된 차세대 온라인 교육 시스템의 콘텐츠 라이프사이클(Content Lifecycle) - 스펙 언어, 제작 툴, 관리 시스템, 플레이어 엔진 등 콘텐츠 관련 총괄 아키텍처 디자인
    *   여러 브라우저에 호환되는 플레이어 엔진 디자인 및 구현
    *   즉각적인 콘텐츠 UX 테스트를 위해 콘텐츠 샌드 박스를 제공하여 복잡한 콘텐츠도 신속하게 제작 가능하게 함

    프로젝트 규모: 35명 참여


    사용한 기술: Nodejs, Hapi Framework, JavaScript, Google Closure tool, Redis. 애자일 방법론

*   **리드 엔지니어, 전자상거래 MVC 프레임워크 도입, Wayfair**, 미국 (2013/02/10 ~ 2013/06/19)

    빠르게 성장한 Wayfair의 시스템은 초창기 때 개발한 ASP 코드가 지배적이었고 대부분이 “스파게티 코드"였습니다. 회사는 ASP 코드를 PHP로 이전하는 과정에 있었습니다. 저는 PHP로 이전하는 작업에서 원래 코드가 안티 패턴(Anti-pattern)으로 가득 차 있다는 것을 깨닫고 MVC (Model-View-Controller) 프레임워크를 구현하여 모든 시스템에 적용해야한다고 제안하였습니다. 초기 설립자 / CTO를 포함하여 기존 관리자로부터 반발이 많았습니다. 아키텍처를 바꾸는 것은 위험 요소가 크고 프레임워크로 인해 시스템의 성능이 저하될 것이라고 여겼기 때문이었습니다. 저는 프레임워크를 제외하고는 페이지 구조가 단순해진 것을 보여주었습니다. 그리고 JMeter를 사용하여 성능이 오히려 향상되었다는 것을 증명하였습니다. 리팩토링 후 모듈 로드 수가 줄어들었기 때문이었습니다. 곧 이 제안은 채택되어 전체 웹과 모바일 전자상거래 시스템에 프레임워크를 적용하는 프로젝트가 되었습니다.


    프레임워크는 점진적으로 웹 커머스 서비스와 모바일 서비스에 배포되었습니다. 


    이 프로젝트에서의 성과는 

    *   시스템 단순화로 유지 보수 성능 향상, 시스템 오류 감소
    *   20% 이상 시스템 성능 향상
    *   새로운 기능을 출시하기까지의 기간 1/4로 단축

    프로젝트 규모: 첫 단계 20명 참여, 그 이후 모든 새로운 개발은 MVC 프레임워크기반으로 구축됨


    사용한 기술: PHP, JMeter, MVC

*   **프로젝트 리드 (Project Lead) / 아키텍트, BBVA은행 서비스 버스 구축, Altenia Corporation**, 파나마 (2010/06 ~ 2010/10)

    파나마의 BBVA 은행은 복합금융 서비스를 구축하는데 시간이 너무 오래 걸린다는 것을 의식하고 개선 방안을 찾고 있었습니다. 서비스들이 이미 구현되어 있는데도 불구하고 그들을 사용하는데 어려움을 겪고 있었던 이유는 서비스들이 API가 없어 소켓 개발이 필요했기 때문입니다.


    BBVA는 위 문제점을 해결하기 위해 저의 회사와 계약을 맺었습니다. 저는 서비스 버스(Service Bus)를 만들고 각 서비스를 SOAP / WSDL 인터페이스로 노출했습니다. 안정적인 커뮤니케이션 계층도 중요했지만 메시지 파싱(Message parsing) 모듈이 핵심 역할을 했습니다. 수십개의 메시지 포맷을 처리하기 위해 메시지 사양과 입력 데이터가 주어지면 XML 메시지로 변환하는 모듈은 새로운 서비스를 쉽게 추가하는 것을 가능하게 하였습니다.


    이 프로젝트에서의 성과는 

    *   표준화된 API 사용으로 새로운 서비스 통합시간 단축
    *   서비스 모니터링 가능해짐

    프로젝트 규모: 12명 참여


    사용한 기술: AS400, WebSphere, IBM DB2, Java, Spring Framework, SOAP

*   **프로젝트 리드(Project Lead) / 아키텍트, 파나마 이동 통신사 감사 시스템 구축**, Autoridad Nacional de Servicios Públicos(Public Service Regulation Bureau), Altenia Corporation, 파나마 (2010/01 ~ 2010/05)

    파나마에 진입한 여러 외국 이동통신사들이 품질은 소홀히 하며 광고로 고객 확보에만 열을 올리는 상황이었습니다. 이에 정부는 소비자 보호 차원에서 통신 품질 감사를 실시하였습니다.


    저희 회사는 파나마 이동통신사의 모바일 서비스 품질지표를 추적하고 분석하는 소프트웨어 시스템을 구축하는 입찰에 선정되었습니다.


    시스템은 MS-IIS에서 실행되는 C#기반, 웹 애플리케이션으로 구성되었습니다. MS SQL 서버를 사용하는 이 애플리케이션은 통신 회사에서 대량 데이터를 로드할 때 비즈니스 룰(Business rule)에 따라 처리하고 기관이 서비스를 감사하는데 사용하는 서비스 품질 보고서를 생성합니다. 프로젝트에서 저의 역할은 개발 팀을 이끌고 설계 및 구현에 참여하는 것이었습니다.


    각각 20개의 서로 다른 품질지표 유형에 따라 입력데이터 형식, 비즈니스 룰 및 보고서 형식이 있습니다. 입력 형식의 메타 데이터가 주어지면 데이터 검사기와 보고서를 생성하는 스키마 관리 모듈을 구현했습니다. 비즈니스 룰은 명령 패턴(Command pattern)을 사용하여 지표 유형간에 재사용이 가능하게 하였습니다. 모든 모듈은 Spring.NET의 DI(Dependency Injection)를 사용하여 통합하였습니다.


    사용한 기술: .NET (C#), MVC.NET, Spring.NET, IIS, SQL Server

*   **프로젝트 리드(Project Lead) / 아키텍트**, 중남미 LG 전자 콜센터 관리시스템, Altenia Corporation, 파나마 (2008/09 ~ 2009/04)

    LG 전자 라틴아메리카는 파나마에 위치한 콜센터를 통해 중남미 12개국의 고객 서비스 지원을 하고 있었습니다. 콜센터는 맞춤형으로 구축된 Oracle 애플리케이션을 사용하여 콜 티켓을 관리하였습니다. 사용되고있던 시스템은 확장성 문제로 에이전트의 능률을 저해하고 있었습니다. 저희 회사는 레거시 시스템을 대체 할 프로그램을 개발해주기로 했습니다.


    웹기반 케이스관리 애플리케이션을 구축하여 기존 애플리케이션을 교체하였습니다. 팀 리더로써 저는 설계에서 개발, 마이그레이션 및 시스템 전환까지 전적으로 참여했습니다.


    애플리케이션은 정시에 제공되었고 고객은 기능에 매우 만족해 했습니다. 새로운 시스템을 통해 관리자는 수신 통화 통계를 실시간으로 볼 수 있어 리소스 할당을 최적화할 수 있었습니다. 저희가 CTI(Computer Telephony Integration)를 추가 기능으로 제안하자 LG는 계약을 확장하여 추가 개발하게 되었습니다. CTI는 통화 정보를 자동으로 추출하고 기존 고객 내역을 검색하여 서비스 품질을 더욱 향상시켰습니다.


    이 프로젝트에서의 성과는 

    *   상세 통계를 이용하여 리소스 최적화가 가능해져 운영비 감축
    *   CTI 통합으로 에이전트의 성능 향상

    사용한 기술: Java, Spring Framework, MySQL, jQuery, C#, CTI

*   **엔지니어**, KT Multimedia Messaging System Enhancement, 누리솔루션, 한국 (2004/07 ~ 2004/10)

    기존 멀티미디어 메시징 시스템(MMS)의 통신 미들웨어를 개선시켜 전체 시스템의 성능과 안정성을 개선시킴 


    사용한 기술: HP UX, C, Java, Message Queue

*   **리드 엔지니어**, 인천 신공항하이웨이 관리시스템 구축, METHODi, 한국 (1999/11 ~ 2000/10)

    신공항하이웨이 관리시스템은 삼성 SDS가 주 SI(System Integrator)로 9개의 하위 시스템으로 이루졌고 각 하위시스템은 외주업체가 개발하였습니다. CORBA를 주요 통신 백본으로 채택하여 외주업체로 합류한 저는 다른 하위 시스템을 통합하는 미들웨어 개발을 담당하는 팀의 리더 역할을 맡았습니다. 


    저는 시스템간 전송되는 모든 메시지에 대해 Interface Definition Language(IDL)을 사용하여 인터페이스를 정의했습니다. 그리고 저희 팀과 함께 각 시스템에 대한 스텁(Stub)와 프록시(Proxy)를 구현하여 각각의 애플리케이션에 통합했습니다. 또한 객체지향 모델에 익숙하지 않은 엔지니어들을 대상으로 교육을 실시하여 CORBA에서 사용되는 OO 패러다임에 대한 능숙도를 향상시켰습니다.


    품질을 보장하고 팀이 독립적으로 작업할 수 있도록 인터페이스 테스트 도구(Interface Test Tool)를 만들어 각 팀에 배포하였습니다. 


    프로젝트에서 사용한 CORBA 제품은 고 가용성(High Availability)기능이 내장되어있지 않았기 때문에 저는 통신상태와 하트 비트의 조합을 통해 통신장애 감지를 구현하고 시스템이 복구될 때까지 메시지를 보관하기 위해 신뢰할 수 있는 로컬 스토리지로 재시도 기능을 구현했습니다. 


    이 프로젝트에서의 성과는 

    *   삼성SDS와 외주업체 엔지니어들 대상으로 객체지향 교육을 실시하여 전체 프로젝트의 개발 품질을 향상시킴
    *   고 가용성(High Availability) 메커니즘의 구현으로 분산된 시스템 중 하나에 오류가 발생하여도 다른 하위시스템은 정상적으로 작동하게 함 

    프로젝트 규모: 60여명 참여 \
사용한 기술: HP UX, Iona Orbix CORBA, C++, Oracle Pro*C/C++, DCOM, PowerBuilder


## 논문 
- Dutt, V., Ahn, Y.S., & Gonzalez, C. (2011). Cyber Situation Awareness: Modeling the Security Analyst in a Cyber-Attack Scenario through Instance-Based Learning. Lecture Notes in Computer Science, 6818, 280-292. doi: 10.1007/978-3-642-22348-8_24
- Dutt, V., Ahn, Y.S.., & Gonzalez, C. (in preparation). Cyber Situation Awareness: Modeling the Detection of Cyber Attacks with Instance-based Learning Theory.
- Dutt, V., Ahn, Y.S.., Ben-Asher, N., & Gonzalez, C. (2012). Modeling the Effects of Base-rates on Cyber Threat Detection Performance. In Paper presented at the 11th International Conference on Cognitive Modeling. Berlin, Germany.
- Dutt, V., Ahn, Y.S.., & Gonzalez, C. (2011). Cyber Situation Awareness: Modeling the Security Analyst in a cyber-attack scenario through Instance-based Learning. In Paper presented at the 25th Annual WG 11.3 Conference on Data and Applications Security and Privacy (to appear in Lecture Notes in Computer Science 6818 (LNCS 6818), Springer) . Richmond, VA, USA.
- Dutt, V., Ahn, Y.S., & Gonzalez, C. (2011). Cyber Situation Awareness: Modeling the Security Analyst in a cyber-attack scenario through Instance-based Learning. In Poster presented at the 20th Behavior Representation in Modeling & Simulation (BRIMS) Conference. Sundance Resort, Utah, USA.
- Nam, J.W., Joung, J.G., Ahn,Y.S., and Zhang, B., Two-Step Genetic Programming for Optimization of RNA Common-Structure, EvoBio 2004, LNCS Journal. (2004.04)
“EAI & Web Service Platform,” Programmer’s World Magazine, May 2002, Korea. (2002.05)


## 자격/수상
- 2011/12 ~ 2012/12, MSE Fellowship, Carnegie Mellon University
- 2010/12, 우수 전문가 국가 장학금, 파나마 정부
- 2001/10, CEO Excellence Award, IONA Technologies

## 컨퍼런스 발표
- “IONA e-Finance Solution, Credit Suisse Case Study,” Seoul Financial IT International Conference. (2001.09.12)
- “IONA Enterprise Portal Solution,” 2001 EIP Solution Fair, Seoul, Korea. (2001.08.30)
- “Total Business Integration,” EAI Solution Fair, Seoul, Korea. (2001.06.15)
- “CORBA 3.0 Specifications,” invited speaker, KRNet99, Seoul, Korea. (1999.05.30)
- “E-Commerce Solutions with CORBA,” presentation, ’99 EC/CALS Technical Workshop, Korea Science & Technology Building. (1999.05.04)
- “Distributed Systems with CORBA,” Korea OpenGIS Workshop ’99. (1999.04.09)
- “CORBA Load Balancing Strategy,” 2nd Domestic Object Oriented Seminar, Seoul, Korea. (1999.02.25~26)
- "Object Oriented System," Lecture for Computer Science Department faculty, Universidad Tecnológica de Panamá, Panama. (1998.02)
