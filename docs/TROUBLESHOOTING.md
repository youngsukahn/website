# Troubleshooting

## Error: error:0308010C:digital envelope routines::unsupported

Execute
```sh
npm audit fix --dev
```

Original error message:
```
node:internal/crypto/hash:69
  this[kHandle] = new _Hash(algorithm, xofLen);
                  ^

Error: error:0308010C:digital envelope routines::unsupported
    at new Hash (node:internal/crypto/hash:69:19)
    at Object.createHash (node:crypto:133:10)
    at module.exports (/Users/ysahnpark/workspace/youngsukahn/website/node_modules/webpack/lib/util/createHash.js:135:53)
    at NormalModule._initBuildHash (/Users/ysahnpark/workspace/youngsukahn/website/node_modules/webpack/lib/NormalModule.js:417:16)
    at handleParseError (/Users/ysahnpark/workspace/youngsukahn/website/node_modules/webpack/lib/NormalModule.js:471:10)
    at /Users/ysahnpark/workspace/youngsukahn/website/node_modules/webpack/lib/NormalModule.js:503:5
    at /Users/ysahnpark/workspace/youngsukahn/website/node_modules/webpack/lib/NormalModule.js:358:12
    at /Users/ysahnpark/workspace/youngsukahn/website/node_modules/loader-runner/lib/LoaderRunner.js:373:3
    at iterateNormalLoaders (/Users/ysahnpark/workspace/youngsukahn/website/node_modules/loader-runner/lib/LoaderRunner.js:214:10)
    at Array.<anonymous> (/Users/ysahnpark/workspace/youngsukahn/website/node_modules/loader-runner/lib/LoaderRunner.js:205:4)
    at Storage.finished (/Users/ysahnpark/workspace/youngsukahn/website/node_modules/enhanced-resolve/lib/CachedInputFileSystem.js:55:16)
    at /Users/ysahnpark/workspace/youngsukahn/website/node_modules/enhanced-resolve/lib/CachedInputFileSystem.js:91:9
    at /Users/ysahnpark/workspace/youngsukahn/website/node_modules/graceful-fs/graceful-fs.js:123:16
    at FSReqCallback.readFileAfterClose [as oncomplete] (node:internal/fs/read_file_context:68:3) {
  opensslErrorStack: [ 'error:03000086:digital envelope routines::initialization error' ],
  library: 'digital envelope routines',
  reason: 'unsupported',
  code: 'ERR_OSSL_EVP_UNSUPPORTED'
}

Node.js v18.19.0
error Command failed with exit code 1.
info Visit https://yarnpkg.com/en/docs/cli/run for documentation about this command.
ERROR: "build:webpack" exited with 1.
error Command failed with exit code 1.
info Visit https://yarnpkg.com/en/docs/cli/run for documentation about this command.
```
