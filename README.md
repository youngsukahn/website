Young Suk Ahn Park's Website
============================

This is my personal website based on [Eleventy](https://www.11ty.dev/) v2. It deploys to GitLab pages through the gitlab-ci.

The source is public but is not intended to be used as template/theme, but for my own use.

Feel free to browse the code, report any issue of any nature (technical, content, grammar, etc).

## Pre-requisite

Account in GitLab, to deploy to GitLab pages.
Nodejs 14+ installed.

If you need to change node version, I suggest you use volta.

```sh
volta install node@18
```

## Running the site

If you are running locally, you will need to pass the empty ROOT_PATH environment, otherwise it will default to "website"

```
$ yarn install
$ ROOT_PATH= yarn serve [--port:<YOUR-PORT>]
```

If you do not see any error, you should be able to open the browser to the address as shown in the output message.
